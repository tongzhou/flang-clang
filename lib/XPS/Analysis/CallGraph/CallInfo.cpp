
#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"
#include "Utils/XPSTypeCache.h"
#include "Utils/PassCommons.h"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<string> Target("node");
static cl::opt<int> Level("level", cl::init(1));
static cl::opt<bool> Distinct("distinct");
static cl::opt<bool> ShowCaller("caller", cl::init(false));
static cl::opt<bool> ShowCallee("callee", cl::init(false));

class CallInfoPass : public ModulePass {
  int _id;
  Module* _m;
  //XPSTypeCache _tc;
public:
  static char ID;
public:
  CallInfoPass(): ModulePass(ID) {
    _id = 0;
  }

  ~CallInfoPass() {

  }


  void queryCallee(Function* F, int lv) {
    if (lv == 0) {
      return;
    }

    std::set<Function*> callees;
    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
          if (callee->isIntrinsic()) {
            continue;
          }

          if (callee->isDeclaration()) {
            continue;
          }

          if (Distinct) {
            if (callees.find(callee) != callees.end())
              continue;
          }
          callees.insert(callee);
          
          outs() << "  " << callee->getName() << "\n";
        }
      }
    }
  }

  void queryCaller(Function* F, int lv) {
    if (lv == 0) {
      return;
    }
    std::set<Function*> callers;
    for (auto U: F->users()) {
      if (auto CS = CallSite(U)) {
        auto I = CS.getInstruction();
        auto caller = I->getFunction();

        if (Distinct) {
          if (callers.find(caller) != callers.end())
            continue;
        }
        
        callers.insert(caller);
        outs().indent((Level-lv)*2) << caller->getName();
        auto loc = I->getDebugLoc();
        if (loc) {
          outs() << " ("
                 << loc->getFilename() << ":"
                 << loc->getLine() << ") ";
        }
        outs() << "\n";
        queryCaller(caller, lv-1);
      }
    }
  }


  void queryAllCaller(Function* F) {
    std::set<Function*> callers;
    size_t c = 0;
    for (auto U: F->users()) {
      if (auto CS = CallSite(U)) {
        c++;
      }
    }
    if (c > 1) {
      outs() << F->getName() << " " << c << "\n";
    }

  }

  bool runOnModule(Module& M) override {
    _m = &M;

    for (auto& FF: M) {
      queryAllCaller(&FF);
    }
    auto F = M.getFunction(Target);
    if (!F) {
      outs() << Target + " not found.\n";
      return false;
    }
    
    if (ShowCaller) {
      outs() << "Show callers\n";
      outs() << "[" << F->getName() + "]\n";
      queryCaller(F, Level);
    }

    if (ShowCallee) {
      outs() << "Show callees\n";
      outs() << "[" << F->getName() + "]\n";

      queryCallee(F, Level);
    }

    return true;
  }
};

char CallInfoPass::ID = 0;

static RegisterPass<CallInfoPass>
    CallInfoPassInfo("call-info", "");

}
