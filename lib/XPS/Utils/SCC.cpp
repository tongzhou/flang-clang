#include <algorithm>
#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>
#include <llvm/Support/raw_ostream.h>
#include <SCC.hpp>

typedef std::string string;

using namespace llvm;

namespace xps {

void SCC::setId(int id) {
  _id = id;
}

void SCC::addNode(Function* node) {
  _nodes.insert(node);
}

std::set<Instruction*> SCC::getIncomingEdges(Function* node) {
  assert(_graph->isDiscovered(node));
  std::set<Instruction*> all_callers;
  std::set<Instruction*> ret_callers;
  get_callers(node, all_callers);
  for (auto I: all_callers) {
    if (!_graph->isValidEdge(CallSite(I))) {
      continue;
    }
    
    if (_graph->isInternalEdge(CallSite(I))) {
      ret_callers.insert(I);
    }
  }
  return ret_callers;
}

std::vector<Instruction*> SCC::getAllEdges() {
  std::vector<Instruction*> edges;
  for (auto node: _nodes) {
    for (auto e: get_callers(node)) {
      if (_graph->getSCCID(e->getFunction()) == _id) {
        edges.push_back(e);
      }
    }
  }
  return edges;
}

std::vector<Instruction*> SCC::getEffectiveEdges() {
  std::set<Instruction*> edges;
  for (auto node: _nodes) {
    int internal_caller_count = 0;
    std::set<Instruction*> cur_callers = _graph->getInternalCallers(node);
    
    // for (auto e: get_callers(node)) {
    //   if (_graph->getSCCID(e->getFunction()) == _id) {
    //     internal_caller_count++;
    //     cur_callers.push_back(e);
    //   }
    // }

    if (cur_callers.size() > 1) {
      for (auto e: cur_callers) {
        edges.insert(e);
      }
    }
  }

  for (auto e: getBackEdges()) {
    edges.insert(e);
  }
  outs() << "back edges in scc " << _id << ": " << getBackEdges().size() << "\n";
  return std::vector<Instruction*>(edges.begin(), edges.end());
}

std::set<Instruction*> SCC::getBackEdges() {
  std::set<Instruction*> back_edges;
  std::set<Function*> _visited;
  for (auto node: _nodes) {
    _visited.insert(node);
    int internal_caller_count = 0;
    std::set<Instruction*> cur_callers = _graph->getInternalCallers(node);
    for (Instruction* e: cur_callers) {
      if (_visited.find(e->getFunction()) != _visited.end()) {
        back_edges.insert(e);
      }
    }
  }
  return back_edges;
}

}
