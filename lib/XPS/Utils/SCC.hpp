//
// Created by tzhou on 3/5/18.
//

#ifndef LLVM_SCC_HPP
#define LLVM_SCC_HPP

#include <algorithm>
#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>
#include <llvm/Support/raw_ostream.h>

typedef std::string string;
//#define DEBUG_MODE

#ifdef DEBUG_MODE
#define DEBUG(s) s
#else
#define DEBUG(s) // s
#endif

using namespace llvm;

namespace xps {

class GenericNode {
  
};

class GenericEdge {
  // GenericNode* getStartNode() = 0;
  // GenericNode* getEndNode() = 0;
};

class GenericDiagraph {
public:
  // std::set<GenericEdge*> getIncomingEdges(GenericNode* node) = 0;
  // std::set<GenericEdge*> getOutgoingEdges(GenericNode* node) = 0;
};

// Think SCC as a graph. Its nodes are SCCNodes. Its edges are SCCEdges.
// In the meantime, SCCs also form another graph, which is SCCGraph
// In SCCGraph, each SCC is a node. Its edges are SCCGraphEdges.
// class SCCNode: GenericNode {
//   Function* _func;
// };

class SCCGraph;

// class SCCEdge: GenericEdge {
//   Instruction* _inst;
// public:
//   GenericNode* getStartNode() {
//     _inst->getFunction()
//   }
// }; 
  
class SCC: GenericNode {
  int _id;
  SCCGraph* _graph;
  std::set<Function*> _nodes;
public:
  SCC(SCCGraph* graph): _graph(graph) {}
  void setId(int id);
  void addNode(Function* node);
  std::set<Function*> getNodes()      { return _nodes; }
  std::set<Instruction*> getBackEdges();
  std::set<Instruction*> getIncomingEdges(Function* node);
  std::vector<Instruction*> getAllEdges();
  std::vector<Instruction*> getEffectiveEdges();
};


class SCCGraph {
public:
  Module *_m;
  int _time;
  int _scc_id;
  std::map<Value*, int> _starts;
  std::map<Value*, int> _finishes;
  std::map<Value*, int> _lowlinks;
  std::vector<Function*> _stack;
  std::map<Function*, bool> _on_stack;
  std::map<Function*, int> _scc_map;
  std::set<int> _simple_sccs;
  std::map<int, SCC*> _scc_groups;
public:
  SCCGraph(Module* m) {
    _time = 0;
    _scc_id = 0;
    _m = m;
  }

  bool isVisited(Value* V) {
    if (_starts.find(V) == _starts.end()) {
      V->dump();
    }
    assert(_starts.find(V) != _starts.end());
    return _starts[V] != -1;
  }

  bool isFinished(Value* V) {
    assert(_finishes.find(V) != _finishes.end());
    return _finishes[V] != -1;
  }

  // Used to filter unused functions.
  // This function is only useful when we only mark SCC
  // from the main function.
  bool isUndiscovered(Function* F) {
    return _scc_map.find(F) == _scc_map.end();
  }

  bool isDiscovered(Function* F) {
    return _scc_map.find(F) != _scc_map.end();
  }

  void markSCC() {
    for (auto& F: *_m) {
      _starts[&F] = -1;
      _finishes[&F] = -1;
    }

    // for (auto& F: *_m) {
    //   if (!has_definition(&F)) {
    //     continue;
    //   }

    //   if (!isVisited(&F)) {
    //     markSCCImpl(&F);
    //   }
    // }
    
    markSCCImpl(get_main_function(_m));


    //getDistinctPathNum();

    verifyTopoSort();
  }

  void getDistinctPathNum() {
    for (auto it: _scc_groups) {
      std::set<Instruction*> counted_callers;
      for (auto& F: it.second->getNodes()) {
        auto callers = getInternalCallers(F);
        if (callers.size() > 1) {
          counted_callers.insert(callers.begin(), callers.end());
        }
      }

      if (!counted_callers.empty()) {
        outs() << "scc: " << it.first << " "
               << "funcs: " << it.second->getNodes().size() << " "
               << "distinct edegs: " << counted_callers.size()
               << "\n";
//        for (auto I: counted_callers) {
//          dump(I);
//        }
        outs() << "\n";
      }

    }
  }

  void setSCCID(Function* F, int id) {
    _scc_map[F] = id;
    if (_scc_groups.find(id) == _scc_groups.end()) {
      _scc_groups[id] = new SCC(this);
      _scc_groups[id]->setId(id);
    }
    _scc_groups[id]->addNode(F);
  }

  SCC* getSCCGroup(int id) {
    return _scc_groups[id];
  }

  // In reverse topological sort
  std::vector<int> getReverseTopoSCCIDs() {
    std::vector<int> ret;
    for (auto i: _scc_groups) {
      ret.push_back(i.first);
    }
    return ret;
  }

  // In topological sort
  std::vector<int> getTopoSCCIDs() {
    std::vector<int> ret;
    for (auto i = _scc_groups.rbegin();
         i != _scc_groups.rend(); ++i) {
      ret.push_back(i->first);
    }
    return ret;
  }

  int getSCCID(Function* F) {
//    if (F->getName().equals("play_ascii_emacs")) {
//      outs() << isUndiscovered(F) << "\n";
//    }
    if (isUndiscovered(F)) {
      //outs() << "Function " << F->getName()
      //     << " level not found\n";
      return -1;
    }
    return _scc_map.at(F);
  }

  void updateLowLink(Function* F, int v) {
    if (_lowlinks[F] > v) {
      _lowlinks[F] = v;
    }
  }

  void markSCCImpl(Function* F) {
    DEBUG(outs() << "now: " << F->getName() << "\n");

    _starts[F] = _time++;
    _lowlinks[F] = _starts[F];
    _stack.push_back(F);
    _on_stack[F] = true;

    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
          if (!callee) {
            continue;
          }

          if (!has_definition(callee)) {
            continue;
          }

          if (!isVisited(callee)) {
            markSCCImpl(callee);
            updateLowLink(F, _lowlinks[callee]);
          }
          else if (_on_stack[callee]) {
            // Successor w is in stack S and hence in the current SCC
            // If w is not on stack, then (v, w) is a cross-edge in the
            // DFS tree and must be ignored
            // Note: The next line may look odd - but is correct.
            // It says w.index not w.lowlink; that is deliberate and from
            // the original paper
            //printCall(CS);
            updateLowLink(F, _starts[callee]);
          }
        }
      }
    }
    
    DEBUG(outs() << F->getName() << " "
           << _lowlinks[F] << " "
           << _starts[F] << "\n";)

    // If v is a root node, pop the stack and generate an SCC
    if (_lowlinks[F] == _starts[F]) {
      while (_stack[_stack.size()-1] != F) {
        auto top = _stack[_stack.size()-1];
        _stack.pop_back();
        _on_stack[top] = false;
        setSCCID(top, _scc_id);
      }
      assert(_stack[_stack.size()-1] == F);
      _stack.pop_back();
      _on_stack[F] = false;
      setSCCID(F, _scc_id);
      _scc_id++;
    }

    _finishes[F] = _time++;
  }

  // void markSimpleSCCs() {
  //   int counter = 0;
  //   assert(_scc_groups.size() == _scc_id);
  //   for (auto it: _scc_groups) {

  //     bool is_simple = true;
  //     //outs() << it.first << "\n";
  //     for (auto F: it.second) {

  //       //outs() << F->getName() << "\n";
  //       if (!hasSingleInternalCaller(F)) {
  //         is_simple = false;
  //       }
  //       //outs() << F->getName() << "\n";
  //     }
  //     //outs() << it.first << "\n";

  //     if (is_simple) {
  //       _simple_sccs.insert(it.first);
  //       counter++;

  //     }
  //   }

  //   outs() << "simple scc: " << counter << "\n";

  // }

  // bool isSimpleSCC(int id) {
  //   return _simple_sccs.find(id) != _simple_sccs.end();
  // }

  // bool isOptimizableSCC(int scc) {
  //   if (isSimpleSCC(scc) && _scc_groups[scc].size() == 1) {
  //     return true;
  //   }
  //   else {
  //     return false;
  //   }
  // }

  bool isEntryNode(Function* F) {
    for (auto caller: get_callers(F)) {
      if (getSCCID(caller->getFunction()) != getSCCID(F)) {
        return true;
      }
    }
    return false;
  }

  bool isExitNode(Function* F) {
    for (auto callee: get_callees(F)) {
      if (getSCCID(callee) != getSCCID(F)) {
        return true;
      }
    }
    return false;
  }

  void getSuccEdges(int id, std::vector<Instruction*>& edges) {
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group->getNodes()) {
      for (auto &B: *F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto callee = get_callee(CS);
            if (!has_definition(callee)) {
              continue;
            }

            if (!isInternalEdge(CS)) {
              edges.push_back(&I);
            }
          }
        }
      }
    }
  }

  std::vector<Instruction*> getSuccEdges(int id) {
    std::vector<Instruction*> edges;
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group->getNodes()) {
      for (auto &B: *F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto callee = get_callee(CS);
            if (!has_definition(callee)) {
              continue;
            }

            if (!isInternalEdge(CS)) {
              edges.push_back(&I);
            }
          }
        }
      }
    }
    return edges;
  }

  void getPredEdges(int id, std::vector<Instruction*>& edges) {
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group->getNodes()) {
      for (auto I: get_callers(F)) {
        if (!isValidEdge(CallSite(I))) {
          continue;
        }

        if (!isInternalEdge(CallSite(I))) {
          edges.push_back(I);
        }
      }
    }
  }

  std::vector<Instruction*> getPredEdges(int id) {
    std::vector<Instruction*> edges;
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group->getNodes()) {
      for (auto I: get_callers(F)) {
        if (!isValidEdge(CallSite(I))) {
          continue;
        }

        // if (id == 9) {
        //   dumpI(I);
        //   outs() << isInternalEdge(CallSite(I)) << "\n";
        // }
        
        if (!isInternalEdge(CallSite(I))) {
          edges.push_back(I);
        }
      }
    }
    return edges;
  }

  void printSCCGroup(int id) {
    outs() << id << ": ";
    //_scc_groups[id]->dump();
    outs() << "\n";
  }

  bool hasSingleSuccessor(int id) {
    std::vector<Instruction*> edges;
    getSuccEdges(id, edges);
    return edges.size() == 1;
  }

  bool hasSinglePredecessor(int id) {
    std::vector<Instruction*> edges;
    getPredEdges(id, edges);
    return edges.size() == 1;
  }

  bool isValidEdge(CallSite CS) {
    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    return isDiscovered(caller);
  }

  bool isInternalEdge(CallSite CS) {
    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    auto callee = get_callee(CS);
    return getSCCID(caller) == getSCCID(callee);
  }

  int getEdgeSCCID(CallSite CS) {
    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    return getSCCID(caller);
  }

  std::set<Instruction*> getInternalCallers(Function* F) {
    assert(isDiscovered(F));
    std::set<Instruction*> all_callers;
    std::set<Instruction*> ret_callers;
    get_callers(F, all_callers);
    for (auto I: all_callers) {
      if (!isValidEdge(CallSite(I))) {
        continue;
      }

      if (isInternalEdge(CallSite(I))) {
        ret_callers.insert(I);
      }
    }
    return ret_callers;
  }

  bool hasSingleInternalCaller(Function* F) {
    return getInternalCallers(F).size() == 1;
  }

  bool hasInternalEdges(int scc) {
    int internal_callers = 0;
    for (auto F: _scc_groups[scc]->getNodes()) {
      internal_callers += getInternalCallers(F).size();
    }

    return internal_callers != 0;
  }

  void verifyTopoSort() {
    std::map<int, bool> visited;
    for (int id = 0; id < _scc_id; ++id) {
      //outs() << "in " << id << "\n";
      std::vector<Instruction*> edges;
      getSuccEdges(id, edges);
      for (auto I: edges) {
        auto callee = get_callee(CallSite(I));
        int callee_scc = getSCCID(callee);
//        outs() << "succ: " << callee_scc << " "
//               << visited[callee_scc] << "\n"
//            ;
        assert(visited[callee_scc] == true);
      }
      visited[id] = true;
      //outs() << "out " << id << "\n";
    }

  }

  void printSCCID() {
    for (auto it: _scc_map) {
      Function* f = it.first;
      assert(f);

      if (!has_definition(f)) {
        outs() << f->getName() << " no definition\n";
      }
      assert(has_definition(f));
      outs() << f->getName() << " "
             << it.second << "\n"
          ;
    }
  }

  size_t getInternalEdgeNum() {
    int counts[2] = {0};
    for (auto&F: *_m) {
      if (isUndiscovered(&F)) {
        continue;
      }

      for (auto& B: F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto caller = I.getFunction();
            auto callee = get_callee(CS);
            if (!callee) {
              continue;
            }

            if (!has_definition(callee)) {
              continue;
            }

            counts[0]++;
            if (_scc_map[caller] == _scc_map[callee]) {
              counts[1]++;
            }
          }
        }
      }
    }

    return counts[1];
  }

  
  void printStats() {
    int counts[10] = {0};
    for (auto&F: *_m) {
      if (isUndiscovered(&F)) {
        continue;
      }

      for (auto& B: F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto caller = I.getFunction();
            auto callee = get_callee(CS);
            if (!callee) {
              continue;
            }

            if (!has_definition(callee)) {
              continue;
            }

            counts[0]++;
            if (_scc_map[caller] == _scc_map[callee]) {
              counts[1]++;
            }
          }
        }
      }
    }

    for (int id = 0; id < _scc_id; ++id) {
      if (hasInternalEdges(id)) {
        counts[2]++;
      }
    }

    outs() << "[scc]:\n";
    outs() << "  SCCs: " << _scc_id << "\n"
      << "  complex SCCs: " << counts[2] << "\n"
           << "  all edges: " << counts[0] << "\n"
           << "  internal edges: " << counts[1] << "\n";
  }
};
}

#endif //LLVM_SCC_HPP
