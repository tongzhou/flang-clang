//
// Created by tzhou on 12/26/17.
//

// @Deprecated
// This code is called in BackendUtil.cpp before to hard-wire
// XPS passes into clang


//#include "clang/XPS/Passes.h"
//#include "clang/XPS/Flags.h"
//#include "Passes/BenPass.h"
//#include "Passes/CtxVarPass.cpp"
////#include "Passes/CtxRecordPass.cpp"
//#include "Passes/CtxVarOptPass.cpp"
//#include "Passes/LogAPPass.cpp"
//#include "Analysis/PointerAnalysis/HelloPointer.cpp"
//#include "Analysis/LoopInfo/DomSetPass.h"
//
//using namespace llvm;
//
//namespace xps {
//int Passes::addXPSModulePasses(legacy::PassManager &PM) {
//  int added = 0;
//  if (Flags::get_bool_flag("UseCVPass")) {
//    PM.add(new xps::CtxVarPass());
//    added++;
//  }
//
//  if (Flags::get_bool_flag("UseTestPass")) {
//    PM.add(new xps::DomSetPass());
//    added++;
//  }
//
//  if (Flags::get_bool_flag("UseLogPass")) {
//    outs() << "Load LogAPPass...\n";
//    PM.add(new xps::LogAPPass());
//    added++;
//  }
//
//  return added;
//}
//
//void Passes::runModulePasses(Module* module) {
//  if (Flags::get_bool_flag("UseBenPass")) {
//    outs() << "Load BenPass...\n";
//    auto pass = new xps::BenPass();
//    pass->run_on_module(module);
//  }
//
//  if (Flags::get_bool_flag("UseCVPass")) {
//    ModulePass* pass = new xps::CtxVarPass();
//    pass->runOnModule(*module);
//  }
//
//  if (Flags::get_bool_flag("UseLogPass")) {
//    outs() << "Load LogAPPass...\n";
//    ModulePass* pass = new xps::LogAPPass();
//    pass->runOnModule(*module);
//  }
//
//
////  if (Flags::get_bool_flag("UseCRPass")) {
////    outs() << "Load CRPass...\n";
////    ModulePass* pass = new CtxRecordPass();
////    pass->runOnModule(*module);
////  }
//
//  if (Flags::get_bool_flag("UseCVOptPass")) {
//    outs() << "Load CVOptPass...\n";
//    ModulePass* pass = new CtxVarOptPass();
//    pass->runOnModule(*module);
//  }
//
//}
//
//ModulePass* Passes::getModulePass(std::string name) {
//  if (name == "CVPass") {
//    return new CtxVarPass();
//  }
//  else {
//    string m = "XPSPass " + name + " not found.";
//    assert(false && m.c_str());
//  }
//}
//
//} // end namespace xps
