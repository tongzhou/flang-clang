//
// Created by tzhou on 12/23/17.
//

#include <cmath>
#include <set>
#include <fstream>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Utils/CallGraphBase.h>
#include "Utils/SCC.hpp"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<string> CCE("cce");
static cl::opt<string> Mode("mode");
static cl::opt<string> ContextFile("context-file");
static cl::opt<bool> PrintStats("print-stats");
// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class DetectionPass: public ModulePass, public XPSTypeCache {
public:
  Module* _m = nullptr;
  SCCGraph* _scc;
  Function* _detector;
  size_t _point_id = 0;
  std::set<size_t> _point_ids;
public:
  static char ID;

  DetectionPass(): ModulePass(ID) {

  }

  ~DetectionPass() {

  }

  void appendNewGlobal() {
    insertFunctionDecls();
  }

  void insertFunctionDecls() {
    std::vector<Type*> arg_types{ Int64Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    _detector = Function::Create(FT, Function::ExternalLinkage, CCE+"_detect_ctx", _m);
  }

  // Like "0xabc"
  size_t parseHexStr(string s) {
    return strtol(s.substr(2).c_str(), NULL, 16);
  }


  void loadDetectionPoints() {
    std::ifstream ifs(ContextFile);
    string line;
    while (std::getline(ifs, line)) {
      int p = line.find(' ');
      string point_id = line.substr(0, p);
      _point_ids.insert(parseHexStr(point_id));
    }
  }

  void instrumentFunctions() {
    // for (int id = _scc->_scc_id-1; id >= 0; --id) {
      
    // }
//    int counter = 0;
//    for (int id = 0; id < _scc->_scc_id; ++id) {
//      for (auto F: _scc->getSCCGroup(id)) {
//        if (!has_definition(F)) {
//          continue;
//        }
//        outs() << F->getName() << "\n";
//        doFunction(F);
//        counter++;
//      }
//
//
//    }
//
    int counter = 0;
    std::vector<Function*> funcs;
    if (Mode == "leaf") {
      funcs = getLeafFunctions();  
    }
    else if (Mode == "cyclic") {
      funcs = getCyclicFunctions();  
    }
    else if (Mode == "random") {
      //assert(!ContextFile.empty());
      //loadDetectionPoints();
      funcs = getRandomFunctions(1);  
    }
    else {
      assert(0);
    }
    
    for (auto f: funcs) {
      doFunction(f);
      counter++;
      // if (_point_id == 30) {
      //   break;
      // }
    }
    outs() << counter << " query clients\n";
  }

  std::set<Function*> getHotFunctions(int k) {
    std::set<Function*> funcs;
    for (auto& F: *_m) {
      if (!has_definition(&F)) {
        continue;
      }

      if (get_defined_callees(&F).empty()) {
        
        funcs.insert(&F);
      }
    }
    return funcs;
  }

  std::vector<Function*> getCyclicFunctions(int k=-1) {
    std::vector<Function*> funcs;
    //funcs.push_back(_m->getFunction("recursive_connect2"));

    int counter = 0;
    for (int id = 0; id < _scc->_scc_id; ++id) {
      if (!_scc->hasInternalEdges(id)) {
        continue;
      }

      for (auto F: _scc->getSCCGroup(id)->getNodes()) {
        if (funcs.size() == k) {
          return funcs;
        }
        
        if (!has_definition(F)) {
          continue;
        }

        outs() << "scc: " << id << ", F: " << F->getName() << "\n";
        assert(std::find(funcs.begin(), funcs.end(), F) == funcs.end());
        funcs.push_back(F);

        
        counter++;

      }
    }
    return funcs;
  }


  std::vector<Function*> getLeafFunctions(int k=-1) {
    std::vector<Function*> funcs;
    for (auto& F: *_m) {
      if (!has_definition(&F)) {
        continue;
      }

      if (get_defined_callees(&F).empty()) {
        //outs() << F.getName() << "\n";
        assert(std::find(funcs.begin(), funcs.end(), &F) == funcs.end());
        funcs.push_back(&F);
      }
    }
    return funcs;
  }

  std::vector<Function*> getRandomFunctions(size_t k) {
    std::vector<Function*> funcs;
    size_t counter = 0;
    for (auto& F: *_m) {
      if (!has_definition(&F)) {
        continue;
      }


      outs() << "random: " << F.getName() << "\n";
      assert(std::find(funcs.begin(), funcs.end(), &F) == funcs.end());
      if (counter%k == 0) {
        funcs.push_back(&F);
      }
        
      counter++;

    }
    return funcs;
  }
  
  BasicBlock* getFirstBB(Function* F) {
    for (auto& B: *F) {
      return &B;
    }
  }

  void doFunction(Function* F) {
    if (F->getBasicBlockList().empty()) {
      return;
    }
    

    auto I = F->front().getFirstNonPHI();
    std::vector<Value*> args {ConstantInt::get(Int64Ty, _point_id)};

    if (!ContextFile.empty()) {
      if (_point_ids.find(_point_id) != _point_ids.end()) {
        //outs() << _point_id << ": " << F->getName() << " " << _scc->getSCCID(F) << "\n";
        CallInst::Create(_detector, args, "", I);
      }
    }
    else {
      // outs() << _point_id << ": " << F->getName() << " " << _scc->getSCCID(F) << "\n";
      CallInst::Create(_detector, args, "", I);
    }
    
    

    _point_id++;
  }

  void printStats() {
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;

    _scc = new SCCGraph(_m);
    _scc->markSCC();
    //_scc->markSimpleSCCs();
    _scc->printStats();
    
    appendNewGlobal();
    instrumentFunctions();

    // if (PrintStats) {
    //   printStats();
    // }

    return true;
  }
};

char DetectionPass::ID = 0;


static RegisterPass<DetectionPass> DetectionPassInfo("insert-detection", "CCECommons Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}


