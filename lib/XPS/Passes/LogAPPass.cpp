//
// Created by tzhou on 12/28/17.
//

#ifndef LLVM_LOGAPPASS_H
#define LLVM_LOGAPPASS_H


#include <set>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/CallSite.h>
#include <fstream>
#include <clang/Frontend/CodeGenOptions.h>
#include "Utils/XPSTypeCache.h"
#include "BenPass.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"

using namespace llvm;

namespace xps {

typedef std::string string;
typedef std::vector<Function*> FuncListType;
typedef std::set<Function*> FuncSetType;

class LogAPPass : public ModulePass, public XPSTypeCache {
public:
  Module* _m;
  int _nlevel = 3;
  bool _extern_only = true;
  bool _print_sites = true;

  static char id;

  LogAPPass() : ModulePass(id) {

    initialize();
  }

  bool initialize() {
    outs().changeColor(raw_ostream::MAGENTA, true);
    outs() << "<LogAPPass WARNING>: Make sure you clear the old log files before doing the LogAP compile run. \n";
    outs().resetColor();
    return false;
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    log_alloc_paths();


    return true;
  }

  void write_sites_to_file(std::set<Instruction*>& insts, std::ofstream& ofs) {
    for (auto I: insts) {
      auto C = CallSite(I);
      assert(C);
      auto caller = I->getFunction();
      DILocation* loc = I->getDebugLoc();

      if (caller->getLinkage() == GlobalValue::ExternalLinkage) {
        ofs << I->getFunction()->getName().str()
            << " " << C.getCalledFunction()->getName().str()
            /* for debug */
            << " " << loc->getFilename().str()
            << " " << loc->getLine()
            << " " << I
          << '\n';
      }
    }
  }

  void log_alloc_paths() {
    std::set<Instruction*> l0_sites, l1_sites, l2_sites;
    std::string layout_comment = "; Field layout: <callerName> <calleeName> <callsite(Instruction*)>\n"
        "; By default, only external functions are recorded here\n"
        "; Make sure you clear this file before doing another LogAP compile run\n";
    std::ofstream ofs;
    get_first_level_callsites(l0_sites);
    ofs.open("L0-callers.txt", std::ios::app);
    write_sites_to_file(l0_sites, ofs);
    ofs.close();

    get_upper_level_callsites(l0_sites, l1_sites);
    ofs.open("L1-callers.txt", std::ios::app);
    write_sites_to_file(l1_sites, ofs);
    ofs.close();

    get_upper_level_callsites(l1_sites, l2_sites);
    ofs.open("L2-callers.txt", std::ios::app);
    write_sites_to_file(l2_sites, ofs);
    ofs.close();
  }

  void get_first_level_callsites(std::set<Instruction*>& sites) {
    auto ben = new BenPass();
    for (auto i: ben->_alloc_set) {
      if (auto F = _m->getFunction(i->old_name)) {
        for (auto U: F->users()) {
          if (auto C = CallSite(U)) {
            Instruction* caller = C.getInstruction();
            sites.insert(caller);
          }
        }
      }
    }
  }

  void get_upper_level_callsites(std::set<Instruction*>& callees, std::set<Instruction*>& callers) {
    for (auto i: callees) {
      Function* callee = i->getFunction();
      for (auto U: callee->users()) {
        if (auto C = CallSite(U)) {
          Instruction* caller = C.getInstruction();
          if (callees.find(caller) != callees.end()) {
            continue;
          }
          callers.insert(caller);
        }
      }
    }
  }

  void print_callsites(std::set<Instruction*>& insts) {
    for (auto I: insts) {
      auto C = CallSite(I);
      assert(C);
      outs() << I << ": " << I->getFunction()->getName()
             << " -> " << C.getCalledFunction()->getName() << '\n';
    }
    outs() << "\n";
  }


};

char LogAPPass::id = 0;

}

#endif //LLVM_LOGAPPASS_H
