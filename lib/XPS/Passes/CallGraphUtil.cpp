//
// Created by tzhou on 1/26/18.
//

#include <cmath>
#include <set>
#include <fstream>
#include <fcntl.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Passes/PCCE/BackEdgeHelper.hpp>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>

using namespace llvm;

namespace xps {

typedef std::string string;
static cl::opt<string> Func("func", cl::init(""));
static cl::opt<bool> ShowCallers("show-callers", cl::init(false));
static cl::opt<bool> ShowCallees("show-callees", cl::init(false));

class CallGraphUtilPass : public ModulePass {
  Module* _m;
public:
  static char ID;
public:
  CallGraphUtilPass(): ModulePass(ID) {}

  void showCallers(Function* F) {
    outs() << "Callers for " << F->getName() << '\n';
    for (auto caller: get_callers(F)) {
      dumpI(caller);
    }
  }

  void showCallees(Function* F) {
    outs() << "Callee for " << F->getName() << '\n';
    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          Function* callee = get_callee(CS);
          if (callee && callee->isIntrinsic()) {
            continue;
          }

          //outs() << "callee: " << callee->getName() << "\n";
          I.dump();
        }
      }
    }
  }

  bool runOnModule(Module& M) override {
    _m = &M;

    Function* F = M.getFunction(Func);
    if (!F) {
      outs() << "Function not found\n";
      return false;
    }

    if (ShowCallees) {
      showCallees(F);
    }

    if (ShowCallers) {
      showCallers(F);
    }

    return false;
  }
};

char CallGraphUtilPass::ID = 0;

static RegisterPass<CallGraphUtilPass>
    CallGraphUtilPassInfo("callgraph", "");

}
