//
// Created by tzhou on 2/25/18.
//

#ifndef LLVM_ECCESTATS_H
#define LLVM_ECCESTATS_H

#include <cstdlib>
#include <vector>
#include <llvm/Support/raw_ostream.h>
#include <iostream>

class SiteStats {
  std::vector<size_t> _counts;
public:
  SiteStats() {
    _counts.assign(4, 0);
  }

  void incTotalCount(size_t v=1) {
    _counts[0] += v;
  }

  void incBECount(size_t v=1) {
    _counts[1] += v;
  }

  void incInstrumentedCount(size_t v=1) {
    _counts[2] += v;
  }

  void incInstrumentedBECount(size_t v=1) {
    _counts[3] += v;
  }

  void reportSiteStats() {
    std::cout << "[edge stats]\n";
    std::cout << "  total edges: " << _counts[0] << "\n"
              << "  back edges: " << _counts[1] << "\n"
              << "  instrumented: " << _counts[2] << "\n"
              << "  instrumented back edges: " << _counts[3] << "\n";
  }
};

#endif //LLVM_ECCESTATS_H
