//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_ACCE_HPP
#define XPS_ACCE_HPP

#include <cmath>
#include <set>
#include <fstream>
#include <fcntl.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Passes/PCCE/BackEdgeHelper.hpp>
#include "ICHelper.hpp"
#include "ECCEStats.hpp"

using namespace llvm;

namespace xps {

#define Baseline
//  #define OffsetedStore

typedef std::string string;

static cl::opt<int> CtxSize("ctx-size", cl::init(128));
static cl::opt<string> QCaller("caller");
static cl::opt<string> QCallee("callee");
static cl::opt<bool> UseRLE("use-rle", cl::init(false));
static cl::opt<bool> DoBE("back-edge", cl::init(true));
static cl::opt<bool> PrintPaths("print-paths");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> DynCallLogger("dyn-logger");

// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class BaseCRPass: public ModulePass, public XPSTypeCache, public SiteStats {
public:
  int _time;
  size_t _count;
  int _max_level;
  bool _verbose = true;
  bool _use_stride = true;
  bool _run_length= false;
  bool _record_cv = false;
  bool _use_ics = false;
  bool _opt_singular_calls = false;
  
  Module* _m = nullptr;
  std::vector<size_t> _ids;
  std::vector<size_t> _eids;
  std::vector<size_t> _counts;
  std::map<Value*, int> _starts;
  std::map<Value*, int> _finishes;
  std::map<Function*, int> _freqs;
  std::map<Function*, int> _func_levels;
  std::set<Instruction*> _back_edges;
  std::map<Value*, int> _scc_map;
  ICHelper* _ic_helper;
  std::set<string> _query_funcs;
  std::ofstream _log;
  
public:
  static char ID;

  BaseCRPass(): ModulePass(ID) {
    _time = 0;
    _count = 0;
    _max_level = 0;
    _counts.assign(2, 0);
  }

  ~BaseCRPass() {

  }

  void initQueryFunctions() {
    _query_funcs.insert("malloc");
    _query_funcs.insert("calloc");
    _query_funcs.insert("realloc");
    _query_funcs.insert("free");
  }

  bool isQueryFunction(Function* F) {
    return _query_funcs.find(F->getName().str())
        != _query_funcs.end();
  }

  bool hasDefinition(Function* F) {
    if (!F) {
      return false;
    }

    if (F->hasExactDefinition()) {
      return true;
    }

    if (isQueryFunction(F)) {
      return true;
    }

    return false;
  }

  bool isSingularCall(CallSite CS) {
    Function* f = CS.getCalledFunction();
    if (!f) {
      return false;
    }
    
    int callers = 0;
    for (auto User: f->users()) {
      callers++;
    }

    return callers == 1;
  }

  bool hasSingleCaller(Function* F) {
    int callers = 0;
    for (auto User: F->users()) {
      callers++;
    }

//    std::set<Instruction*> ic_callers;
//    _ic_helper->getICCallers(F, ic_callers);
//    callers += ic_callers.size();

    return callers == 1;
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }

  bool hasSingleCallee(Function* F) {
    int callsites = 0;
    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          callsites++;
        }
      }
    }
    return callsites == 1;
  }

  void setFunctionLevel(Function* F, int L) {
    _func_levels[F] = L;
    if (L > _max_level) {
      _max_level = L;
    }
  }

  void queryFunctionLevel() {
    if (!QCaller.empty()) {
      Function* F = _m->getFunction(QCaller);
      if (F) {
        outs() << QCaller << " " << _func_levels[F]
               << "\n";
      }
    }

    if (!QCallee.empty()) {
      Function* F = _m->getFunction(QCallee);
      if (F) {
        outs() << QCallee << " " << _func_levels[F]
               << "\n";
      }
    }
  }

  void calcFunctionLevels() {
    for (auto& F: *_m) {
      _freqs[&F] = 0;
      _func_levels[&F] = -1;
    }

    auto main = get_main_function(_m);
    assert(main);
    calcFunctionLevelsImpl(main, 0);

    for (auto& F: *_m) {
      //outs() << F.getName() << " " << _func_levels[&F] << "\n";
    }
  }

  raw_ostream& OS(int L) {
    for (int i = 0; i < L; ++i) {
      errs() << "  ";
    }
    return errs();
  }

  void calcFunctionLevelsImpl(Function* F, int L) {
    _freqs[F]++;
    if (PrintPaths) {
      OS(L) << "> " << F->getName()
          << " (" << L << ") "
          << _freqs[F]
          << '\n';
    }

    if (L <= _func_levels[F]) {
      goto FINISH;
    }

    setFunctionLevel(F, L);
    if (F->isDeclaration()) {
      goto FINISH;
    }

    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
            
          if (!hasDefinition(callee)) {
            continue;
          }

          //outs() << "callee: " << callee->getName() << "\n";

          if (isBackEdge(&I)) {
            continue;
          }

          int next_level = L + 1;
          if (hasSingleCallee(F)) {
            next_level = L;
          }

          if (hasSingleCaller(callee)) {
            next_level = L;
          }
          calcFunctionLevelsImpl(callee, next_level);

        }
      }
    }

    FINISH:
    if (PrintPaths) {
      OS(L) << "< " << F->getName()
            << " (" << L << ")"
            << '\n';    
    }
  }

  void instrumentCallSites() {
    _ids.assign(CtxSize, 1);
    _eids.assign(_max_level, 1);

    for (auto it: _func_levels) {
      auto f = it.first;
      auto lv = it.second;
      if (lv == -1) { /* no path to it */
        continue;
      }
      for (auto& B: *it.first) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            doCallSite(CS);
          }
        }
      }
    }
  }

  void queryCallSite(Instruction* I,
                     Function* callee) {
    Function* caller = I->getFunction();
    int caller_level = _func_levels.at(caller);
    int callee_level = _func_levels.at(callee);
    outs() << caller->getName() << " "
           << caller_level << " -> ";
    outs() << callee->getName() << " "
           << callee_level;
    outs() << "\n";

    DILocation* loc = I->getDebugLoc();
    if (loc) {
      outs() << "loc: "
             << loc->getFilename() << ":"
             << loc->getLine() << "\n";
    }

    int i = caller_level;
    for (; i < callee_level; ++i) {
      outs() << i << " ";
      outs().write_hex(_ids[i]);
      outs() << "\n";
    }
  }

  int getCallerLevel(CallSite CS) {
    auto I = CS.getInstruction();
    Function* caller = I->getFunction();
    return _func_levels[caller];
  }

  int getCalleeLevel(CallSite CS) {
    auto callee = get_callee(CS);
    return _func_levels[callee];
  }

  void doBackEdge(CallSite CS) {
    if (!DoBE) {
      return;
    }

    auto I = CS.getInstruction();
    int l1 = getCallerLevel(CS);
    int l2 = getCalleeLevel(CS);


    auto callee = get_callee(CS);
    Function* caller = I->getFunction();
    if (callee == caller) {
      outs() << caller->getName() << "\n";
    }
    outs() << "BE: " << l1 << " " << l2 << "\n";
    assert(l1 >= l2);
    if (l1 == l2) {
      //assert(I->getFunction() == get_callee(CS));
    }

    _counts[0]++;
    incInstrumentedCount();
    incInstrumentedBECount();
    instrumentOneLevel(I, l1);

    std::vector<Value*> args;
    std::vector<Value*> args1;


#ifdef OffsetedStore
    int distance = l1 + 1 - l2;
    outs() << "BE distance: " << distance << "\n";
    int bytes = distance * sizeof(short);
    if (distance <= 4) {
      args.push_back(ConstantInt::get(Int16Ty, l2));
      Function* xps_push_ctx = _m->getFunction("acce_push_short_ctx");
      CallInst::Create(xps_push_ctx, args, "", I);

      args1.push_back(ConstantInt::get(Int16Ty, l2));
      Function* xps_pop_ctx = _m->getFunction("acce_pop_short_ctx");
      CallInst::Create(xps_pop_ctx, args1, "", I->getNextNode());
    }
    else {
      args.push_back(ConstantInt::get(Int16Ty, l2));
      args.push_back(ConstantInt::get(Int16Ty, distance));
      CallInst::Create(_m->getFunction("acce_push_long_ctx"),
                       args, "", I);

      CallInst::Create(_m->getFunction("acce_pop_long_ctx"),
                       args, "", I);
    }
    return;
//    args.push_back(ConstantInt::get(Int16Ty, l2));
//    args.push_back(ConstantInt::get(Int16Ty, distance));
//    args.push_back(ConstantInt::get(Int32Ty, bytes));
//
//    args1.push_back(ConstantInt::get(Int32Ty, bytes));
//
//    Function* xps_push_ctx = _m->getFunction("acce_push_ctx");
//    CallInst::Create(xps_push_ctx, args, "", I);
//
//    Function* xps_pop_ctx = _m->getFunction("acce_pop_ctx");
//    CallInst::Create(xps_pop_ctx, args1, "", I->getNextNode());
#endif


#ifdef Baseline
    int bytes = (l1+1) * sizeof(short);
    args.push_back(ConstantInt::get(Int32Ty, bytes));
    args1.push_back(ConstantInt::get(Int32Ty, bytes));
    Function* xps_push_ctx = _m->getFunction("acce_push_ctx");
    CallInst::Create(xps_push_ctx, args, "", I);

    Function* xps_pop_ctx = _m->getFunction("acce_pop_ctx");
    CallInst::Create(xps_pop_ctx, args1, "", I->getNextNode());
#endif

  }

  void doCallSite(CallSite CS) {
    auto callee = get_callee(CS);
//    if (_use_ics) {
//      callee = _ic_helper->getHighestCallee(
//          _func_levels, CS);
//    }
    if (!hasDefinition(callee)) {
      return;
    }

    auto I = CS.getInstruction();
    if (isBackEdge(I)) {
      doBackEdge(CS);
      return;
    }

    Function* caller = I->getFunction();

    if (caller->getName().equals(QCaller) &&
        callee->getName().equals(QCallee)) {
      queryCallSite(I, callee);
    }

    int caller_level = _func_levels.at(caller);
    int callee_level = _func_levels.at(callee);

    assert(caller_level <= callee_level);
    if (caller_level == callee_level) {
      return;
    }

    incInstrumentedCount();
    int distance = callee_level - caller_level;

    if (UseRLE && distance > 4) {
      doRunLengthEncoding(CS);
    }
    else {
      int i = caller_level;
      if (_use_stride) {
        /* Store by 64 bits if more than 4 layers */
        while (callee_level - i >= 4) {
          instrumentFourLevels(I, i);
          i += 4;
        }
        
        if (i+4 <= CtxSize) {
          instrumentFourLevels(I, i);
          i += 4;
          assert(i >= callee_level);

        }
        
        /* Store by 64 bits if more than 2 layers */
        while (callee_level - i >= 2) {
          instrumentTwoLevels(I, i);
          i += 2;
        }
      }

      /* Store by 16 bits for the rest */
      for (; i < callee_level; ++i) {
        instrumentOneLevel(I, i);
      }
    }

    /* number of instrumented sites */
    _counts[0]++;
  }

  void doRunLengthEncoding(CallSite CS) {
    auto I = CS.getInstruction();
    int l1 = getCallerLevel(CS);
    int l2 = getCalleeLevel(CS);
    setControlCtx(I, l1, l2);
  }

  void setControlCtx(Instruction* I, int l1, int l2) {
    auto gep = insertGEP(I, l1);
    auto bc = new BitCastInst(gep, Int64Ty->getPointerTo(0),
                              "", I);
    short values[4];
    // [Layout]
    // slot l1+0: _ids[l1]
    // slot l1+1: 0 (control char)
    // slot l1+2: l2 - l1
    values[0] = _ids[l1];
    values[1] = 0;
    values[2] = l2 - l1;
    values[3] = 0;
    auto c = ConstantInt::get(Int64Ty,
                              *((size_t *)values));
    auto store = new StoreInst(c, bc, I);

    _ids[l1]++;
    _counts[1]++;
  }

  void setExtendedCtx(Instruction* I, int l) {
    auto gep = insertExtCtxGEP(I, l);
    auto c = ConstantInt::get(Int16Ty, _eids[l]);
    auto store = new StoreInst(c, gep, I);
    _eids[l]++;
    _counts[1]++;
  }

  GetElementPtrInst* insertGEP(Instruction* I, int level) {
    auto ctx_var = _m->getGlobalVariable("xps_ctx");
    Value* idx[2];
    idx[0] = ConstantInt::get(Int32Ty, 0);
    idx[1] = ConstantInt::get(Int32Ty, level);
    auto gep = GetElementPtrInst::Create(nullptr, ctx_var,
                                         idx, "", I);
    return gep;
  }

  GetElementPtrInst* insertExtCtxGEP(Instruction* I, int level) {
    auto ctx_var = _m->getGlobalVariable("xps_ctx1");
    Value* idx[2];
    idx[0] = ConstantInt::get(Int32Ty, 0);
    idx[1] = ConstantInt::get(Int32Ty, level);
    auto gep = GetElementPtrInst::Create(nullptr, ctx_var,
                                         idx, "", I);
    return gep;
  }

  string getNameFromLevel(int level, int stride) {
    string s = "ctx";
    for (int i = level; i < level+stride; ++i) {
      s += "_" + std::to_string(i);
      char hex[10];
      sprintf(hex, "%04x", (short)_ids[i]);
      s += "_";
      s += hex;
    }
    return s;
  }

  void instrumentFourLevels(Instruction* I, int level) {
    auto gep = insertGEP(I, level);
    auto bc = new BitCastInst(gep, Int64PtrTy, "", I);
    size_t level_id = getFourLevelID(level);
    auto c = ConstantInt::get(Int64Ty, level_id);
    auto store = new StoreInst(c, bc, I);
    bc->setName(getNameFromLevel(level, 4));

    for (int i = 0; i < 4; ++i) {
      _ids[level+i]++;
    }
    _counts[1]++;
  }

  void instrumentTwoLevels(Instruction* I, int level) {
    auto gep = insertGEP(I, level);
    auto bc = new BitCastInst(gep, Int32Ty->getPointerTo(0), "", I);
    auto c = ConstantInt::get(Int32Ty,
                              getCompoundID(level, 2));
    auto store = new StoreInst(c, bc, I);
    bc->setName(getNameFromLevel(level, 2));

    for (int i = 0; i < 2; ++i) {
      _ids[level+i]++;
    }
    _counts[1]++;
  }

  size_t getFourLevelID(int l) {
    size_t id = _ids[l]
        + (_ids[l+1] << 16)
        + (_ids[l+2] << 32)
        + (_ids[l+3] << 48)
    ;
    return id;
  }
  
  size_t getCompoundID(int l, int stride) {
    size_t id = 0;
    for (int i = 0; i < stride; ++i) {
      id += (_ids[l+i] << 16*i);
    }
    return id;
  }

  GlobalVariable* addGlobalStr(Twine name, Twine value) {
    Constant *c = ConstantDataArray::getString(_m->getContext(),
                                               value.str());
    auto *gv = new GlobalVariable(*_m, c->getType(), true,
                                  GlobalValue::ExternalLinkage,
                                  c, name);
    return gv;
  }

  void insertLogCall(Instruction* I, int level) {
    Function* F = I->getFunction();
    auto gv_name = "xps." + F->getName().str();
    auto gv = _m->getGlobalVariable(gv_name);
    if (!gv) {
      gv = addGlobalStr(gv_name, F->getName());
    }

    auto bc = new BitCastInst(gv, Int8PtrTy, "", I);
    Value* v1 = ConstantInt::get(Int32Ty, level);
    Value* v2 = ConstantInt::get(Int16Ty, _ids[level]);
    std::vector<Value*> args_vec {bc, v1, v2};
    Function* callee = _m->getFunction("ctx_log");
    CallInst::Create(callee, args_vec, "", I);
  }

  void insertBackTrace(Instruction* I) {
    std::vector<Value*> args_vec
    { ConstantInt::get(Int32Ty, 12345)};
    Function* callee = _m->getFunction("printBacktrace");
    CallInst::Create(callee, args_vec, "", I);
  }

  void instrumentOneLevel(Instruction* I, int level) {
    insertGEPStore(I, level);
    if (DynCallLogger) {
      insertLogCall(I, level);
    }

    _ids[level]++;
    _counts[1]++;
  }
  
  void insertGEPStore(Instruction* I, int level) {
    auto gep = insertGEP(I, level);
    auto c = ConstantInt::get(Int16Ty, _ids[level]);
    auto store = new StoreInst(c, gep, I);
    gep->setName(getNameFromLevel(level, 1));
  }

  size_t getLevelValue(size_t value, int level) {
    int shift = level * 16;
    return (value << shift);
  }

  void appendFuncDecls() {
    insertPushAndPopDecl();
  }

  void appendNewGlobals() {
    ArrayType *ty = ArrayType::get(Int16Ty, CtxSize);
//    auto gv = new GlobalVariable(*_m, ty, false,
//                                 GlobalValue::ExternalLinkage,
//                                 nullptr, "xps_ctx", nullptr,
//                                 GlobalVariable::GeneralDynamicTLSModel);
    auto gv = new GlobalVariable(*_m, ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "xps_ctx", nullptr);

    if (DynCallLogger) {
      insertLoggerDecl();
      insertBTDecl();
    }
  }

  void insertLoggerDecl() {
    std::vector<Type*> arg_types{ Int8PtrTy, Int32Ty, Int16Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "ctx_log", _m);
  }

  void insertBTDecl() {
    std::vector<Type*> arg_types{ Int32Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "printBacktrace", _m);
  }

  void insertPushAndPopDecl() {
    std::vector<Type*> arg_types{ Int32Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "acce_push_ctx", _m);

    std::vector<Type*> arg_types1{ Int32Ty };
    FunctionType *FT1 = FunctionType::get(VoidTy, arg_types1, false);
    Function::Create(FT1, Function::ExternalLinkage, "acce_pop_ctx", _m);

    std::vector<Type*> arg_types2{ Int16Ty };
    FunctionType *FT2 = FunctionType::get(VoidTy, arg_types2, false);
    Function::Create(FT2, Function::ExternalLinkage, "acce_push_short_ctx", _m);
    std::vector<Type*> arg_types3{ Int16Ty };
    FunctionType *FT3 = FunctionType::get(VoidTy, arg_types3, false);
    Function::Create(FT3, Function::ExternalLinkage, "acce_pop_short_ctx", _m);

    std::vector<Type*> arg_types4{ Int16Ty, Int16Ty };
    Function::Create(FunctionType::get(VoidTy, arg_types4, false),
                     Function::ExternalLinkage, "acce_push_long_ctx", _m);
    std::vector<Type*> arg_types5{ Int16Ty, Int16Ty };
    Function::Create(FunctionType::get(VoidTy, arg_types5, false),
                     Function::ExternalLinkage, "acce_pop_long_ctx", _m);
  }

  void printQueryFuncStats() {
    for (auto name: _query_funcs) {
      Function* f = _m->getFunction(name);
      if (!f) {
        continue;
      }
      outs() << "[" << name << "]\n"
             << "  level: " << _func_levels[f] << "\n"
             << "  count: " << _freqs[f] << '\n'
          ;
    }
  }

  void printIDStats() {
    outs() << "[ids]\n";
    for (int i = 0; i < _ids.size(); ++i) {
      outs() << "  " << i << ": " << _ids[i] << '\n';
    }
  }

  void printBackEdgeStats() {
    size_t num = _back_edges.size();
    outs() << "[back edge]\n";
    outs() << "  " << num << '\n';
  }

  void printStats() {
    printIDStats();
    errs() << "max: " << _max_level << "\n";
    errs() << "size: " << CtxSize << "\n";
    errs() << "sites: " << _counts[0] << "\n";
//    size_t id_count = 0;
//    for (auto i: _ids) {
//      id_count += i;
//    }
//    errs() << "instrumented: " << id_count << "\n";
    errs() << "actual stores: " << _counts[1] << "\n";

    reportSiteStats();
    printBackEdgeStats();
    printQueryFuncStats();
    printStatsToFile();

    string name = "do_owl_defend";
    errs() << name << ": " << _func_levels[_m->getFunction(name)] << "\n";
//    std::vector<size_t> nums(_max_level+1, 0);
//    for (auto it: _func_levels) {
//      Function* f = it.first;
//      int l = it.second;
//      nums[l]++;
//    }

//    outs() << "========= Stats =========\n";
//    for (int i = 0; i < nums.size(); ++i) {
//      outs() << "level " << i
//             << " " << nums[i]
//             << "\n";
//    }
//    outs() << "=========================\n";
  }

  void printStatsToFile() {
    FILE* f = fopen("cr-funcs.txt", "w");
    raw_fd_ostream OS(fileno(f), true);
    for (auto it: _func_levels) {
      Function* f = it.first;
      int l = it.second;
      if (l == -1) {
        continue;
      }
      OS << f->getName() << " " << l << "\n";
    }
    OS.close();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    
//    SCCGraph scc(_m);
//    scc.markSCC(_scc_map);
//    scc.printSCCID();
//    scc.printStats();
//    return true;
    BackEdgeHelper beh;
    beh.markBackEdge(_back_edges, M);


    initQueryFunctions();
    calcFunctionLevels();
    queryFunctionLevel();
    appendNewGlobals();
    appendFuncDecls();
    instrumentCallSites();

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char BaseCRPass::ID = 0;


static RegisterPass<BaseCRPass> BaseCRPassInfo("acce", "Array-based Calling Context Encoding Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif
