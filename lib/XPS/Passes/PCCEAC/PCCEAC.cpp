//
// Created by tzhou on 4/16/18.
//


#include <cmath>
#include <set>
#include <fstream>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <fcntl.h>
#include "../PCCE/BackEdgeHelper.hpp"
#include "../PCCE/TopoSort.hpp"
#include <Utils/CallGraphBase.h>
#include "Utils/SCC.hpp"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<bool> PrintNumCC("print-num-cc");

// General Options
static cl::opt<bool> Baseline("baseline");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> CheckMode("check-mode", cl::init(false));

class PCCEACPass: public ModulePass, public XPSTypeCache {
public:
  int _time = 0;
  int _max_level = 0;
  int _be_counter = 0;
  Type* _ctx_id_ty;
  bool _verbose = true;
  bool _opt_zero_inc = true;

  Module* _m = nullptr;
  SCCGraph* _scc;
  std::vector<size_t> _ids;
  std::vector<size_t> _counts;
  std::vector<Function*> _topo;
  std::map<int, double> _num_cc;
  std::map<int, int> _ctx_levels;
  std::set<Instruction*> _back_edges;
  std::set<int> _root_nodes;
  std::map<Function*, int> _callers;
  std::set<string> _query_funcs;
  std::ofstream _log;

public:
  static char ID;

  PCCEACPass(): ModulePass(ID) {
    _counts.assign(10, 0);

    if (Baseline) {
      _opt_zero_inc = false;
    }
  }

  ~PCCEACPass() {

  }

  void initQueryFunctions() {
    _query_funcs.insert("malloc");
    _query_funcs.insert("calloc");
    _query_funcs.insert("realloc");
    _query_funcs.insert("free");
  }

  bool isQueryFunction(Function* F) {
    return _query_funcs.find(F->getName().str())
        != _query_funcs.end();
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }
  
  void annotateRecursive() {
    int callers = 0;
    auto main = get_main_function(_m);
    assert(main);
    _root_nodes.insert(_scc->getSCCID(main));

    double max_num_cc = 0;
    for (auto id: _scc->getTopoSCCIDs()) {
      _num_cc[id] = 0;
      if (_root_nodes.find(id) != _root_nodes.end()) {
        _num_cc[id]++;
      }

      // if (id == 9) {
      //   for (auto I: _scc->getPredEdges(id)) {
      //     dumpI(I);
      //   }
      // }
            
      //outs() << "id: " << id << ", callers: " << _scc->getPredEdges(id).size()  << "\n";

      for (auto I: _scc->getPredEdges(id)) {
        auto p = I->getFunction();
        _num_cc[id] += _num_cc[_scc->getSCCID(p)];
        //dumpI(I);
      }

      if (_num_cc[id] > max_num_cc) {
        max_num_cc = _num_cc[id];
      }

      if (PrintNumCC) {
        outs() << id << " " << _num_cc[id] << "\n";
      }

      outs() << "id: " << id << ", numcc: " << (size_t)_num_cc[id] << "\n";
    }

    outs() << "max cc num: " << max_num_cc << " " << int(ceil(log2(max_num_cc))) << " numccsize: " << _num_cc.size() << "\ncc bytes:";

    if (max_num_cc < 256) {
      outs() << "(1 byte)\n";
    }
    else if (max_num_cc < 65536) {
      outs() << "(2 bytes)\n";
    }
    else if (max_num_cc < 4294967296) {
      outs() << "(4 bytes)\n";
    }
    else {
      outs() << "(8 bytes)\n";
    }
  }

//  void annotate() {
//    for (auto& F: *_m) {
//      _num_cc[&F] = 0;
//    }
//
//    auto main = get_main_function(_m);
//    assert(main);
//    _num_cc[main] = 1;
//
//    for (int i = _topo.size()-1; i >= 0; --i) {
//      auto f = _topo[i];
//      //outs() << f->getName() << " ";
//      for (auto I: get_callers(f)) {
//        auto p = I->getFunction();
//        _num_cc[f] += _num_cc[p];
//      }
//      //outs() << _num_cc[f] << "\n";
//    }
//  }

  void instrumentCallSites() {
    for (auto id: _scc->getTopoSCCIDs()) {
      if (_num_cc[id] == 0) {
        continue;
      }

      size_t s = 0;
      if (_root_nodes.find(id) != _root_nodes.end()) {
        s = 1;
      }
      
      for (auto I: _scc->getPredEdges(id)) {
        auto pid = _scc->getSCCID(I->getFunction());
        
        doCallSite(CallSite(I), s);
        s += _num_cc[pid];
      }
    }

  }

  bool shouldInstrument(CallSite CS) {
    return true;
  }

  void doCallSite(CallSite CS, size_t s) {
    auto callee = get_callee(CS);
    if (!has_definition(callee)) {
      return;
    }

    auto I = CS.getInstruction();

    if (CS.isIndirectCall()) {
      outs() << I->getFunction()->getName() << " -> "
             << callee->getName() << "\n";
    }
    assert(!isBackEdge(I));

    if (s == 0 && _opt_zero_inc) {
      return;
    }

    Function* caller = I->getFunction();
    insertIncrement(I, s);


    for (auto SI: get_succ_insts(I)) {
      insertDecrement(SI, s);
    }


    /* number of instrumented sites */
    _counts[0]++;
    _counts[1]++;
  }

  void insertIncrement(Instruction* I, size_t value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto add = BinaryOperator::Create(Instruction::Add,
                                      load, v, "", I);
    auto store = new StoreInst(add, ctx_id, I);
  }

  void insertDecrement(Instruction* I, size_t value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto sub = BinaryOperator::Create(Instruction::Sub,
                                      load, v, "", I);
    auto store = new StoreInst(sub, ctx_id, I);
  }

  void appendNewGlobal() {
    auto gv = new GlobalVariable(*_m, _ctx_id_ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "pcce_id", nullptr);
    insertPushAndPopDecl();
  }

  void insertPushAndPopDecl() {
    std::vector<Type*> arg_types{ Int64Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "pcce_push_ctx", _m);

    std::vector<Type*> arg_types1{ };
    FunctionType *FT1 = FunctionType::get(VoidTy, arg_types1, false);
    Function::Create(FT1, Function::ExternalLinkage, "pcce_pop_ctx", _m);

  }

  void printStats() {
    errs() << "max: " << _max_level << "\n";
    errs() << "sites: " << _counts[0] << "\n";
    errs() << "pushes: " << _counts[2] << "\n";

  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;

    _scc = new SCCGraph(_m);
    _scc->markSCC();
    //_scc->markSimpleSCCs();

    _ctx_id_ty = Int64Ty;
    TopoSort ts;
    ts.sort(_topo, M);

    initQueryFunctions();
    BackEdgeHelper beh;
    beh.markBackEdge(_back_edges, M);
    beh.report();

    appendNewGlobal();
    annotateRecursive();
    instrumentCallSites();

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char PCCEACPass::ID = 0;


static RegisterPass<PCCEACPass> PCCEACPassInfo("pcce-ac", "PCCE Pass",
                                           false /* Only looks at CFG */,
                                           false /* Analysis Pass */);

}
