//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_SCCE_HPP
#define XPS_SCCE_HPP

#include <cmath>
#include <set>
#include <fstream>
#include <fcntl.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Passes/PCCE/BackEdgeHelper.hpp>
#include "Utils/SCC.hpp"
#include "Passes/BenPass.h"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<int> PushSize("push-size", cl::init(8));
static cl::opt<bool> OptCycle("opt-cycle", cl::init(false));
static cl::opt<bool> NaivePush("naive-push", cl::init(false));
static cl::opt<bool> Baseline("baseline");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> DynCallLogger("dyn-logger");
static cl::opt<bool> CheckMode("check-mode");
static cl::opt<int> CyclicMode("cyclic-mode", cl::init(2));
static cl::opt<bool> ReplaceAllocs("replace-allocs");
static cl::opt<bool> MinimalSlots("opt-slots");
static cl::opt<bool> InsertBranch("insert-branch", cl::init(false));
static cl::opt<string> InternalEdgeCoding("internal-edge-coding");

static cl::opt<bool> PrintLevels("print-levels");
static cl::opt<bool> PrintStores("print-stores");

class SCCECyclicPass: public ModulePass, public XPSTypeCache {
public:
  int _max_level = 0;
  bool _use_internal_edge_coding = false;

  Module* _m = nullptr;
  SCCGraph* _scc = nullptr;
  int _cycle_slot_bits = 0;
  std::vector<size_t> _counts;
  std::map<int, int> _scc_levels;
  std::ofstream _log;
  std::map<int, string> _coding_map;

public:
  static char ID;

  SCCECyclicPass(): ModulePass(ID) {
    _counts.assign(10, 0);
    if (!InternalEdgeCoding.empty()) {
      _use_internal_edge_coding = true;

      loadInternalEdgeCodingFile();

    }
  }

  ~SCCECyclicPass() {

  }

  void replaceAllocs() {
    insert_new_allocs(_m, "scce_", true);
    BenPass ben;
    for (auto mf: ben._alloc_set) {
      if (auto F = _m->getFunction(mf->old_name)) {
        if (_scc->isUndiscovered(F)) {
          continue;
        }

        outs() << mf->old_name << " "
               << _scc_levels[_scc->getSCCID(F)]
               << "\n";

        for (auto I: get_callers(F)) {
          CallSite CS = CallSite(I);
          if (dyn_cast<InvokeInst>(I)) {
            //I->dump();

            continue;
          }

          // this is okay but cannot add argument directly
          //CS.setCalledFunction(callee1);


          std::vector<Value*> args_vec {
              ConstantInt::get(Int32Ty, _scc_levels[_scc->getSCCID(F)])
          };
          for (int h = 0; h < CS.getNumArgOperands(); ++h) {
            args_vec.push_back(CS.getArgOperand(h));
          }

          Function* callee = _m->getFunction("scce_"+mf->old_name);
          CallInst *new_inst = CallInst::Create(callee, args_vec, "");
          //new_inst->setAttributes(CS.getAttributes());
          new_inst->copyMetadata(*I);
          if (I->getDebugLoc()) {
            new_inst->setDebugLoc(I->getDebugLoc());
          }

          BasicBlock::iterator ii(I);
          ReplaceInstWithInst(I->getParent()->getInstList(), ii, new_inst);
        }
      }
    }
  }

  void setCycleSlotSize(int num) {
    _cycle_slot_bits = 0;

    if (MinimalSlots) {
      for (int i = 0; i < 32; ++i) {
        if (num > pow(2, i)) {
          _cycle_slot_bits = i;
        }
      }

      _cycle_slot_bits++;
    }
    else {
      _cycle_slot_bits = 16;

    }
    
  }

  void instrumentCallSites() {
    int iedge_count = 0;
    int iedge_id = 0;
    setCycleSlotSize(_scc->getInternalEdgeNum());
    printf("internal edge slot: %d (%d slots) %d\n",
           _cycle_slot_bits, getBufferCapacity(), _scc->getInternalEdgeNum());

    for (int id = _scc->_scc_id-1; id >= 0; --id) {
      std::vector<Instruction *> edges;
      _scc->getPredEdges(id, edges);
      int count = 1;

      auto iedges = getInternalCallSites(id);
      if (OptCycle) {
        iedges = getEffectiveInternalCallSites(id);
        outs() << "effective iedge num: " << iedges.size() << "\n";
      }      

      if (!iedges.empty()) {
        for (auto I: iedges) {
          doInternalCallSite(CallSite(I), iedge_id++);
        }
      }

    }

    outs() << "internal sites: " << _counts[3] << "\n";
  }

  void doInternalCallSite(CallSite CS, int iedge_id) {
    Instruction* I = CS.getInstruction();
    if (CyclicMode == 1) {
      if (PushSize == 2) {
        insertPushAndPop(I, iedge_id);
      }
      else {
        insertInlinedPushes(I, iedge_id);
      }
    }
    if (CyclicMode == 2) {
      if (NaivePush) {
        insertShiftCalls(I, iedge_id);
      }
      else {
        // default case
        insertInlinedShifts(I, iedge_id);
      }
    }
    else {

    }
    _counts[3]++;
  }

  std::vector<Instruction*> getEffectiveInternalCallSites(int scc) {
    std::vector<Instruction*> sites;
    return _scc->getSCCGroup(scc)->getEffectiveEdges();
    
  }
  

  std::vector<Instruction*> getInternalCallSites(int scc) {
    return _scc->_scc_groups[scc]->getAllEdges();
  }

  void insertPushAndPop(Instruction* I, int edge_id) {
    std::vector<Value*> args{ ConstantInt::get(Int16Ty, edge_id) };
    Function* F = _m->getFunction("scce_push_edge");
    CallInst::Create(F, args, "", I);

    std::vector<Value*> args1{ };
    Function* F1 = _m->getFunction("scce_pop_edge");
    for (auto SI: get_succ_insts(I)) {
      CallInst::Create(F1, args1, "", SI);
    }

  }

  void insertShiftCalls(Instruction* I, int edge_id) {
    auto needed_bits = getSlotBits(edge_id);
    auto add_value = getEncoding(edge_id);

    std::vector<Value*> args{
      ConstantInt::get(Int32Ty, needed_bits),
        ConstantInt::get(Int32Ty, add_value)
        };
    Function* F = _m->getFunction("scce_left_shift");
    CallInst::Create(F, args, "", I);

    std::vector<Value*> args1{ ConstantInt::get(Int32Ty, needed_bits) };
    Function* F1 = _m->getFunction("scce_right_shift");
    for (auto SI: get_succ_insts(I)) {
      CallInst::Create(F1, args1, "", SI);
    }

  }

  /// Before:
  ///   Head
  ///   CallSite (I)
  ///   Tail
  /// After:
  ///   Head
  ///   if (NeedAlloc)
  ///     ThenBlock
  ///   SetSlot
  ///   CallSite
  ///   Tail
  void insertInlinedPushes(Instruction* I, int edge_id) {
    // First insert ins to calculate branch condition
    auto scce_size = _m->getGlobalVariable("scce_size");
    auto scce_capacity = _m->getGlobalVariable("scce_capacity");
    IRBuilder<> irb(I);
    auto icmp = irb.CreateICmpEQ(new LoadInst(scce_size, "", I),
                                 new LoadInst(scce_capacity, "", I));
    //auto icmp = new ICmpInst(I, ICmpInst::ICMP_EQ, scce_size, scce_capacity)
    // IR structure before split
    // Head
    // Icmp
    // CallSite
    // Tail
    auto check_term = SplitBlockAndInsertIfThen(icmp, I, false);
    auto then_block = check_term->getParent();
    BasicBlock* tail = check_term->getSuccessor(0);
    insertAllocCall(check_term);
    insertStoreSlot(I, edge_id);
  }

  void insertStoreSlot(Instruction* I, int edge_id) {
    assert(edge_id < 65536);
    // ================== Before the call ==================
    // Store edge id to *scce_size
    auto scce_size_ptr = _m->getGlobalVariable("scce_size");
    auto scce_size = new LoadInst(scce_size_ptr, "", I); // char*
    auto c = ConstantInt::get(Int16Ty, edge_id);

    IRBuilder<> irb(I);
    auto base = new LoadInst(_m->getGlobalVariable("scce_start"), "", I);
    auto gep = irb.CreateInBoundsGEP(nullptr, base, scce_size);
    auto store = new StoreInst(c, gep, I);
    //insertStoreCall(I, edge_id);

    // Increment scce_size
    auto c1 = ConstantInt::get(Int64Ty, 1);
    auto add = BinaryOperator::Create(Instruction::Add,
                                      scce_size, c1, "", I);
    new StoreInst(add, scce_size_ptr, I);

    // ================== After the call ==================
    // Decrement scce_size
    for (auto SI: get_succ_insts(I)) {
      scce_size = new LoadInst(scce_size_ptr, "", SI); // char*

      c1 = ConstantInt::get(Int64Ty, 1);
      auto sub = BinaryOperator::Create(Instruction::Sub,
                                        scce_size, c1, "", SI);
      new StoreInst(sub, scce_size_ptr, SI);

//      // Store 0 to *scce_size
//      auto c = ConstantInt::get(Int16Ty, 0);
//      auto store = new StoreInst(c, gep, SI);
    }

  }

  int getBufferCapacity() {
    return 64 / _cycle_slot_bits;
  }

  /**
   * ======== Before =======
   * foo();
   *
   * ======== After ========
   * if (scce_cycle_ctx_size == max) {
   *   scce_push_cycle_ctx(edge_id);
   *   foo();
   *   scce_pop_cycle_ctx();
   * }
   * else {
   *   tmp = scce_cycle_ctx << slot_bits;
   *   scce_cycle_ctx = tmp + edge_id;
   *   scce_cycle_ctx_size += 1;
   *   foo();
   *   scce_cycle_ctx >>= slot_bits;
   *   scce_cycle_ctx_size -= 1;
   * }
   *
   *
   * @param I
   * @param edge_id
   */
  void insertInlinedShifts(Instruction* I, int edge_id) {
    auto needed_bits = getSlotBits(edge_id);
    auto add_value = getEncoding(edge_id);
    // outs() << "edge id: " << edge_id << " "
    //        << "needed bits: " << needed_bits << " "
    //        << "add value: " << add_value << "\n";

    
    if (!InsertBranch) {
      insertPreCallHandler(I, needed_bits, add_value);
      for (auto SI: get_succ_insts(I)) {
        insertPostCallHandler(SI, needed_bits);
      }
      return;
    }
    
    
    // First insert ins to calculate branch condition
    auto scce_cycle_ctx = _m->getGlobalVariable("scce_cycle_ctx");
    auto scce_cycle_ctx_size = _m->getGlobalVariable("scce_cycle_ctx_size");

    IRBuilder<> irb(I);
    auto available_bits = irb.CreateSub(
        ConstantInt::get(Int64Ty, 64),
    irb.CreateLoad(getGV("scce_cycle_ctx_size"))
    );


    auto icmp = irb.CreateICmpUGT(
                                  ConstantInt::get(Int64Ty, needed_bits),
                                  available_bits
                                  
        );
    //auto icmp = new ICmpInst(I, ICmpInst::ICMP_EQ, scce_size, scce_capacity)
    // IR structure before split
    // Head
    // Icmp
    // CallSite
    // Tail
    TerminatorInst *ThenTerm, *ElseTerm;
    SplitBlockAndInsertIfThenElse(icmp, I, &ThenTerm, &ElseTerm);

    auto then_block = ThenTerm->getParent();
    auto else_block = ElseTerm->getParent();

    auto call1 = I->clone();
    auto call2 = I->clone();
    insertThenTermCode(ThenTerm, call1, needed_bits, add_value);
    insertElseTermCode(ElseTerm, call2, needed_bits, add_value);

    if (I->getType()->isVoidTy()) {
      I->eraseFromParent();
    }
    else {
      auto phi = PHINode::Create(call1->getType(), 2);
      phi->addIncoming(call1, call1->getParent());
      phi->addIncoming(call2, call2->getParent());
      ReplaceInstWithInst(I, phi);
    }

    assert(!dyn_cast<InvokeInst>(I));
  }

  void loadInternalEdgeCodingFile() {
    std::ifstream ifs(InternalEdgeCoding);
    string line;
    while (std::getline(ifs, line)) {
      int space1 = line.find(' ');
      int edge_id = std::stoi(line.substr(0, space1));
      string coding = line.substr(space1+1);
      std::cout << "parse " << edge_id << " " << coding << '\n';
      _coding_map[edge_id] = coding;
    }
  }

  void insertThenTermCode(Instruction* term, Instruction* orig_call,
                          int bits, int value) {
    CallInst::Create(
        _m->getFunction("scce_push_cycle_ctx"),
        std::vector<Value*>{
          ConstantInt::get(Int32Ty, bits),
            ConstantInt::get(Int32Ty, value)
            },
        "",
        term
    );
    orig_call->insertBefore(term);

    if (dyn_cast<CallInst>(orig_call)) {
      CallInst::Create(
          _m->getFunction("scce_pop_cycle_ctx"),
          std::vector<Value*>{},
          "",
          term
      );
    }
    else if (dyn_cast<InvokeInst>(orig_call)) {
      for (auto SI: get_succ_insts(orig_call)) {
        CallInst::Create(
            _m->getFunction("scce_pop_cycle_ctx"),
            std::vector<Value*>{},
            "",
            SI
        );
      }

    }
    else {
      assert(0);
    }
  }

  int getSlotBits(int edge_id) {
    if (_coding_map.find(edge_id) != _coding_map.end()) {
      string coding = _coding_map[edge_id];
      int shift_bits = coding.size();
      return shift_bits;
    }
    else {
      return _cycle_slot_bits;
    }
  }

  int getEncoding(int edge_id) {
    if (_coding_map.find(edge_id) != _coding_map.end()) {
      string coding = _coding_map[edge_id];
      int shift_value = std::stoi(coding, nullptr, 2);
      return shift_value;
    }
    else {
      return edge_id;
    }
  }

  /**
   * ======== After ========
   * if (scce_cycle_ctx_size == max) {
   *   scce_push_cycle_ctx(edge_id);
   *   foo();
   *   scce_pop_cycle_ctx();
   * }
   * else {
   *   tmp = scce_cycle_ctx << slot_bits;
   *   scce_cycle_ctx = tmp + edge_id;
   *   scce_cycle_ctx_size += 1;
   *   foo();
   *   scce_cycle_ctx >>= slot_bits;
   *   scce_cycle_ctx_size -= 1;
   * }
   * @param I
   * @param orig_call
   * @param edge_id
   */
  void insertElseTermCode(Instruction* term, Instruction* orig_call,
                          int bits, int value) {
    IRBuilder<> builder(term);
    auto shl = builder.CreateShl(
        builder.CreateLoad(getGV("scce_cycle_ctx")),
        ConstantInt::get(Int64Ty, bits)
    );
    // store (add i64 shl, i64 %edge_id), i64* %scce_cycle_ctx
    auto store = builder.CreateStore(
        builder.CreateAdd(shl, ConstantInt::get(Int64Ty, value)),
        getGV("scce_cycle_ctx")
    );

    auto store1 = builder.CreateStore(
        builder.CreateAdd(
            builder.CreateLoad(getGV("scce_cycle_ctx_size")),
            ConstantInt::get(Int64Ty, bits)
        ),
        getGV("scce_cycle_ctx_size")
    );

    orig_call->insertBefore(term);

    std::set<Instruction*> successors;
    if (dyn_cast<CallInst>(orig_call)) {
      successors.insert(term);
    }
    else if (dyn_cast<InvokeInst>(orig_call)) {
      successors = get_succ_insts(orig_call);
      //term->removeFromParent();
    }
    else {
      assert(0);
    }

    for (auto SI: successors) {
      IRBuilder<> builder(SI);
      auto shr = builder.CreateLShr(
          builder.CreateLoad(getGV("scce_cycle_ctx")),
          ConstantInt::get(Int64Ty, bits)
      );

      // store (add i64 shr, i64 %edge_id), i64* %scce_cycle_ctx
      builder.CreateStore(
          shr,
          getGV("scce_cycle_ctx")
      );

      builder.CreateStore(
          builder.CreateSub(
              builder.CreateLoad(getGV("scce_cycle_ctx_size")),
              ConstantInt::get(Int64Ty, bits)
          ),
          getGV("scce_cycle_ctx_size")
      );
    }

  }

  /**
   * if (scce_cycle_ctx_size == max) {
   *   scce_push_cycle_ctx();
   * }
   *
   * tmp = scce_cycle_ctx << slot_bits;
   * scce_cycle_ctx = tmp + edge_id;
   * scce_cycle_ctx_size += 1;
   * foo();
   *
   * @param I
   * @param edge_id
   */
  void insertPreCallHandler(Instruction* I, int needed_bits, int value) {
    auto scce_cycle_ctx = _m->getGlobalVariable("scce_cycle_ctx");
    auto scce_cycle_ctx_size = getGV("scce_cycle_ctx_size");

    // IRBuilder<> irb(I);
    // auto available_bits = irb.CreateSub(
    //     ConstantInt::get(Int64Ty, 64),
    //     irb.CreateLoad(getGV("scce_cycle_ctx_size"))
    //                                     );

    // auto icmp = irb.CreateICmpUGT(
    //                               ConstantInt::get(Int64Ty, needed_bits),
    //                               available_bits
    //                               );

    IRBuilder<> builder(I);
    auto icmp = builder.CreateICmpEQ(
        new LoadInst(scce_cycle_ctx_size, "", I),
        ConstantInt::get(Int64Ty, getBufferCapacity())
    );

    auto check_term = SplitBlockAndInsertIfThen(icmp, I, false);
    auto then_block = check_term->getParent();
    CallInst::Create(
        _m->getFunction("scce_push_cycle_ctx"),
        std::vector<Value*>{
          ConstantInt::get(Int32Ty, needed_bits),
            ConstantInt::get(Int32Ty, value)
            },
        "",
        check_term
    );

    IRBuilder<> builder1(I);
    auto shl = builder1.CreateShl(
        builder1.CreateLoad(getGV("scce_cycle_ctx")),
        ConstantInt::get(Int64Ty, _cycle_slot_bits)
    );
    // store (add i64 shl, i64 %edge_id), i64* %scce_cycle_ctx
    auto store = builder1.CreateStore(
        builder1.CreateAdd(shl, ConstantInt::get(Int64Ty, value)),
        getGV("scce_cycle_ctx")
    );

    auto store1 = builder1.CreateStore(
        builder1.CreateAdd(
            builder1.CreateLoad(getGV("scce_cycle_ctx_size")),
            ConstantInt::get(Int64Ty, 1)
        ),
        getGV("scce_cycle_ctx_size")
    );
  }

  /**
   * foo();
   * if (scce_cycle_ctx_size == 0) {
   *   scce_pop_cycle_ctx();
   * }
   * scce_cycle_ctx >>= slot_bits;
   * scce_cycle_ctx_size -= 1;
   *
   *
   * @param I
   * @param edge_id
   */
  void insertPostCallHandler(Instruction* I, int shift_bits) {
    auto scce_cycle_ctx = _m->getGlobalVariable("scce_cycle_ctx");
    auto scce_cycle_ctx_size = _m->getGlobalVariable("scce_cycle_ctx_size");


    IRBuilder<> builder(I);
    auto icmp = builder.CreateICmpEQ(new LoadInst(scce_cycle_ctx_size, "", I),
                                     ConstantInt::get(Int64Ty, 0));

    auto check_term = SplitBlockAndInsertIfThen(icmp, I, false);
    auto then_block = check_term->getParent();
    CallInst::Create(
        _m->getFunction("scce_pop_cycle_ctx"),
        std::vector<Value*>{},
        "",
        check_term
    );

    IRBuilder<> builder1(I);
    auto shr = builder1.CreateLShr(
        builder1.CreateLoad(getGV("scce_cycle_ctx")),
        ConstantInt::get(Int64Ty, _cycle_slot_bits)
    );

    // store (add i64 shl, i64 %edge_id), i64* %scce_cycle_ctx
    builder1.CreateStore(
        shr,
        getGV("scce_cycle_ctx")
    );

    builder1.CreateStore(
        builder1.CreateSub(
            builder1.CreateLoad(getGV("scce_cycle_ctx_size")),
            ConstantInt::get(Int64Ty, 1)
        ),
        getGV("scce_cycle_ctx_size")
    );


  }

  GlobalVariable* getGV(string name) {
    return _m->getGlobalVariable(name);
  }


  void insertAllocCall(Instruction* I) {
    std::vector<Value*> args{ };
    Function* F = _m->getFunction("scce_alloc");
    CallInst::Create(F, args, "", I);
  }

  void insertPushCycleCtxCall(Instruction* I) {
    std::vector<Value*> args{ };
    Function* F = _m->getFunction("scce_push_cycle_ctx");
    CallInst::Create(F, args, "", I);
    IRBuilder<> builder(I);

    // store (add i64 shl, i64 %edge_id), i64* %scce_cycle_ctx
    auto store = builder.CreateStore(
        ConstantInt::get(Int64Ty, getBufferCapacity()),
        getGV("scce_cycle_ctx_size")
    );
  }


  void appendNewGlobals() {
    // Add one more param GlobalVariable::GeneralDynamicTLSModel
    // to make it thread-local
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_stack_sizes", nullptr);
    new GlobalVariable(*_m, Int16Ty->getPointerTo(0), false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_start", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_size", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_capacity", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_cycle_ctx", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_cycle_ctx_size", nullptr);
    insertFuncDecl();
  }

  void insertFuncDecl() {
    std::vector<Type*> void_arg_type{ };
    std::vector<Type*> int16_arg_type{ Int16Ty };
    std::vector<Type*> int32_arg_type{ Int32Ty };
    std::vector<Type*> int32_32_arg_type{ Int32Ty, Int32Ty };

    FunctionType *FT_void_void = FunctionType::get(VoidTy, void_arg_type, false);
    FunctionType *FT_void_int16 = FunctionType::get(VoidTy, int16_arg_type, false);
    FunctionType *FT_void_int32 = FunctionType::get(VoidTy, int32_arg_type, false);
    FunctionType *FT_void_int32_32 = FunctionType::get(VoidTy, int32_32_arg_type, false);

    Function::Create(FT_void_void, Function::ExternalLinkage, "scce_alloc", _m);
    Function::Create(FT_void_int32, Function::ExternalLinkage, "scce_store", _m);
    Function::Create(FT_void_int16, Function::ExternalLinkage, "scce_push_edge", _m);
    Function::Create(FT_void_void, Function::ExternalLinkage, "scce_pop_edge", _m);
    Function::Create(FT_void_int32_32, Function::ExternalLinkage, "scce_left_shift", _m);
    Function::Create(FT_void_int32, Function::ExternalLinkage, "scce_right_shift", _m);
    Function::Create(FT_void_int32_32, Function::ExternalLinkage, "scce_push_cycle_ctx", _m);
    Function::Create(FT_void_void, Function::ExternalLinkage, "scce_pop_cycle_ctx", _m);
  }

  void insertInitCode() {
    Function* main = get_main_function(_m);
    assert(main);
    auto I = main->front().getFirstNonPHI();
  }

  void printStats() {
    outs() << "[stats]\n";
    outs() << "  max: " << _max_level << "\n";
    outs() << "  sites: " << _counts[0] << "\n";
    outs() << "  tree edge stores: " << _counts[1] << "\n";
    outs() << "  cycle edge stores: " << _counts[3] << "\n";
    //outs() << "  simple sccs: " << _counts[2] << "\n";


    //printBackEdgeStats();
    //printStatsToFile();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    _scc = new SCCGraph(_m);
    _scc->markSCC();
    //_scc->markSimpleSCCs();
    //_scc->printSCCID();
    _scc->printStats();


    appendNewGlobals();
    instrumentCallSites();

    if (ReplaceAllocs) {
      replaceAllocs();
    }

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char SCCECyclicPass::ID = 0;


static RegisterPass<SCCECyclicPass> SCCECyclicPassInfo(
    "scce-cyclic", "Slot-based Calling Context Encoding Pass",
    false /* Only looks at CFG */,
    false /* Analysis Pass */);
}

#endif
