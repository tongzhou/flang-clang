#include <Utils/PassCommons.h>

using namespace llvm;

namespace xps {
class SCC {
  int _id;
public:
  std::vector<Function*> getFunctions();
  std::set<Instruction*> getBackEdges();
  std::set<Instruction*> getInternalCallers();
};
}
