//
// Created by tzhou on 1/25/18.
//

#ifndef LLVM_CTXVAROPTPASS_H
#define LLVM_CTXVAROPTPASS_H


#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"
#include "../Analysis/LoopInfo/LoopInfo.cpp"

using namespace llvm;

namespace xps {

typedef std::string string;

class CtxVarOptPass : public ModulePass {
  Module* _m;
public:
  static char ID;
public:
  CtxVarOptPass(): ModulePass(ID) {}

  void doFunction(Function& F) {
    LoopInfoPass loopinfo;
    loopinfo.runOnFunction(F);
    for (auto it: loopinfo._loops) {
      auto header = it.first;
      auto L = it.second;
      doCodeMotion(L);
    }
  }

  void doCodeMotion(NaturalLoop* L) {
    auto cv = _m->getGlobalVariable("xps_ctx");
    assert(cv);
    int counter = 0;
    GetElementPtrInst* gep;
    for (auto& B: L->getBlocks()) {
      for (auto& I: *B) {
        if (auto i = dyn_cast<GetElementPtrInst>(&I)) {
          Value* pointer = i->getPointerOperand();
          if (pointer == cv) {
            gep = i;
            counter++;
          }
        }
      }
    }

    if (counter != 1) {
      return;
    }

    auto header = L->getHeader();
    auto preheaders = L->getPreheaders();
    if (preheaders.size() != 1) {
      return;
    }

    auto preheader = preheaders[0];
    auto TI = preheader->getTerminator();
    auto& list = preheader->getInstList();
    if (TI) {
      BasicBlock::iterator pos(TI);
      auto new_gep = gep->clone();
      list.insert(pos, new_gep);
      auto store = dyn_cast<StoreInst>(
          gep->getNextNode());
      assert(store);
      Value* value = store->getValueOperand();

      store->eraseFromParent();
      gep->eraseFromParent();

      new StoreInst(value, new_gep, TI);
      //TI->dump();
      //header->dump();
    }
  }
  
  int getLoopCtxNum(NaturalLoop* L) {
    auto cv = _m->getGlobalVariable("xps_ctx");
    int counter = 0;
    for (auto& B: L->getBlocks()) {
      for (auto& I: *B) {
        if (auto i = dyn_cast<GetElementPtrInst>(&I)) {
          Value* pointer = i->getPointerOperand();
          if (pointer == cv) {
            counter++;
          }
        }
      }
    }
    return counter;
  }

  bool runOnModule(Module& M) override {
    _m = &M;
    for (auto& F: M) {
      if (F.isDeclaration() || F.isIntrinsic()) {
        continue;
      }
      
      doFunction(F);
      
    }
    return true;
  }
};

char CtxVarOptPass::ID = 0;

static RegisterPass<CtxVarOptPass>
    CtxVarOptPassInfo("cv-opt", "");

}

#endif //LLVM_CTXVAROPTPASS_H
