//
// Created by tzhou on 1/26/18.
//

#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"

using namespace llvm;

namespace xps {

typedef std::string string;
static cl::opt<string> OldPrefix("old-prefix", cl::init(""));
  static cl::opt<string> Prefix("prefix", cl::init("ctx_"));
  
class ReplaceCallPass : public ModulePass {
  Module* _m;
public:
  static char ID;
public:
  ReplaceCallPass(): ModulePass(ID) {}

  void doCallReplacement(Module& M) {
    std::vector<string> func_names {
        "malloc", "calloc", "realloc", "free",
        "_Znam", "_Znwm", "_Znaj",
        "_Znwj", "_ZdaPv", "_ZdlPv"
    };

    for (auto name: func_names) {
      if (Function* F = M.getFunction(OldPrefix+name)) {
        F->setName(Prefix+name);
      }
    }

  }

  bool runOnModule(Module& M) override {
    doCallReplacement(M);
    return true;
  }
};

char ReplaceCallPass::ID = 0;

static RegisterPass<ReplaceCallPass>
    ReplaceCallPassInfo("replace-call", "");

}
