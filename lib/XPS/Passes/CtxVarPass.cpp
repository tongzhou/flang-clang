//
// Created by tzhou on 12/23/17.
//

#ifndef LLVM_CtxVar_H
#define LLVM_CtxVar_H

#include <cmath>
#include <set>
#include <llvm/Support/raw_ostream.h>
#include <fstream>
#include <llvm/Support/FileSystem.h>
#include "clang/Frontend/CodeGenOptions.h"
#include "Utils/XPSTypeCache.h"
#include "BenPass.h"
#include "Utils/PassCommons.h"

using namespace llvm;

namespace xps {

typedef std::string string;

class CtxVarPass: public ModulePass, public XPSTypeCache {
public:
  Module* _m = nullptr;
  int _nlevel = 3;
  std::vector<size_t> _site_ids = {0, 0, 0, 0};
  std::map<BasicBlock*, std::set<Instruction*>> _bb;
  std::map<Function*, int> _callers;
  std::ofstream _log;
  bool _init_cv = true;
  bool _verbose = false;
  bool _record_cv = false;
  bool _replace_calls = true;
  bool _opt_singular_calls = true;
  bool _opt_reloads = false;

public:
  static char ID;

  CtxVarPass(): ModulePass(ID) {
    outs() << "Construct CtxVarPass...\n";
    initialize();
  }
  
  ~CtxVarPass() {
    outs() << "Destruct CtxVarPass...\n";
    if (_record_cv) {
      _log.close();
    }
  }

  bool initialize() {
    // Option: nlevel now means the layers of contexts
    //         nlevel=0: no context;
    //         nlevel=1: 1 layer of context; etc.

    outs() << "nlevel: " << _nlevel << "\n";

    return false;
  }

  void getTargetFunctions(Module& module, std::set<Function*>& funcs) {
    BenPass* ben = new BenPass();
    for (auto i: ben->_alloc_set) {
      getCallers(module, i->old_name, funcs, _nlevel);
    }
  }

  void getCallers(Module& module, string& alloc, std::set<Function*>& funcs,
                   int nlevel) {
    Function* alloc_f = module.getFunction(alloc);

    if (!alloc_f) {
      return;
    }

    std::set<Function*> old_set;
    old_set.insert(alloc_f);

    while (nlevel--) {
      std::set<Function*> new_set;
      for (auto f: old_set) {
        for (auto u: f->users()) {
          if (CallInst* ci = dyn_cast<CallInst>(u)) {
            new_set.insert(ci->getFunction());
          }
        }
        funcs.insert(f);
      }

      old_set = new_set;
    }

    for (auto i: old_set) {
      funcs.insert(i);
    }
  }

  size_t getCV() {
    size_t cv = _site_ids[0]
        + (_site_ids[1] << 16)
        + (_site_ids[2] << 32)
        + (_site_ids[3] << 48)
    ;
    return cv;
  }

  size_t getLevelVar(int level) {
    return (_site_ids[level] << (16*level));
  }

  void printStats() {
    int counter = 0;
    for (auto it: _callers) {
      outs() << it.first->getName() << " " << it.second << '\n';
      if (it.second == 1) {
        counter++;
      }
    }

    outs() << _callers.size() << " " << counter << '\n';
    for (int i = 0; i < 4; ++i) {
      outs() << "_site_ids[" << i << "]: " << _site_ids[i] << "\n";
    }
  }

  void addBBInstrumentInfo(Instruction* I) {
    BasicBlock* BB = I->getParent();
    if (_bb.find(BB) == _bb.end()) {
      _bb[BB] = std::set<Instruction*>();
    }

    _bb[BB].insert(I);
  }

  void printBBInstrumentInfo() {
    for (auto it: _bb) {
      BasicBlock* BB = it.first;
      auto sites = it.second;
      outs() << "\n " << sites.size() << '\n';
    }
  }

  size_t getLevelClearMask(int level) {
    int mask;
    switch (level) {
    case 0: return 0xffffffffffff0000;
    case 1: return 0xffffffff0000ffff;
    case 2: return 0xffff0000ffffffff;
    case 3: return 0x0000ffffffffffff;
    }
  }

  size_t getLevelSetMask(int level, size_t value) {
    int shift = level * 16;
    return (value << shift);
  }

  Instruction* getFirstInst(BasicBlock* BB) {
    for (auto& I: *BB) {
      return &I;
    }
  }

  CallInst* createPrintfCall(string literal, std::vector<Value*>& args,
                             Instruction* insert_before) {
    Constant *c = ConstantDataArray::getString(_m->getContext(), literal);
    auto *gv = new GlobalVariable(*_m, c->getType(), true,
                                  GlobalValue::ExternalLinkage, c, "");

    auto casted_value = new BitCastInst(gv, Int8PtrTy, "", insert_before);
    args.insert(args.begin(), casted_value);
    Function* callee = _m->getFunction("printf");
    CallInst *new_inst = CallInst::Create(callee, args, "", insert_before);
    return new_inst;
  }

  CallInst* createPrintCVAddrCalls(Instruction* insert_before) {
    string str = "cv: %p\n";
    std::vector<Value*> args;
    args.push_back(_m->getGlobalVariable("xps_ctx"));
    return createPrintfCall(str, args, insert_before);
  }

  void instrumentCallsites(std::set<Instruction*>& sites, int level) {
    auto ctx_var = _m->getGlobalVariable("xps_ctx");
    assert(ctx_var);
    for (auto I: sites) {
      CallSite S = CallSite(I);
      assert(S && "I should be a callsite");
      if (_opt_singular_calls) {
        if (isSingularCall(S)) {
          continue;
        }
      }
      Instruction* insert_point = S.getInstruction();
      /* doesn't need a name at this point */

      auto load = new LoadInst(ctx_var, "", insert_point);
      if (_verbose) {
        outs() << I->getFunction()->getName() << *I << '\n';
        outs() << "site ids: ";
        for (auto it = _site_ids.rbegin(); it != _site_ids.rend(); ++it) {
          outs() << *it << " ";
        }
        outs() << '\n';
        outs() << "cv: " << getCV() << ", level: " << level << '\n';
      }
      auto clear_mask = ConstantInt::get(Int64Ty,
                                         getLevelClearMask(level));
      auto and_ins = BinaryOperator::Create(Instruction::And,
                                            load, clear_mask, "",
                                            insert_point);
      auto set_mask = ConstantInt::get(Int64Ty,
                                       getLevelSetMask(level,
                                                       _site_ids[level]));
      auto or_ins = BinaryOperator::Create(Instruction::Or,
                                           and_ins, set_mask, "",
                                           insert_point);
      auto store = new StoreInst(or_ins, ctx_var, insert_point);
      
      //Test Area
      //===---------------------------------------------------------------===//
      //createPrintCVAddrCalls(insert_point);
      
      // auto test_int = ConstantInt::get(Int64Ty, 1);
      
      // auto store = new StoreInst(test_int, ctx_var, insert_point);
      //===---------------------------------------------------------------===//
      
//      auto site_id = ConstantInt::get(Int64Ty, 1000000000000);
//      auto add = BinaryOperator::Create(Instruction::Add, load, site_id, "", insert_point);
//      auto store = new StoreInst(add, ctx_var, insert_point);
//

      /* Invoke is an exit instruction, and two invokes could share the same
       * normal and unwind destinations, FIXME
       */

      /* Do some bookkeepping */
      if (_record_cv) {
        _log << level
             << " " << getLevelSetMask(level, _site_ids[level])
             << " " << I->getFunction()->getName().str()
             << "\n";

      }

//      addBBInstrumentInfo(I);

      _site_ids[level]++;
    }
  }

  void appendNewGlobal(Module& M) {
    auto gv = new GlobalVariable(M, Int64Ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "xps_ctx", nullptr,
                                 GlobalVariable::GeneralDynamicTLSModel);
//    auto gv = new GlobalVariable(M, Int64Ty, false,
//                                 GlobalValue::ExternalLinkage,
//                                 nullptr, "xps_ctx", nullptr);
    if (_init_cv) {
      gv->setInitializer(ConstantInt::get(Int64Ty, 0));
    }
  }

  bool isSingularCall(CallSite CS) {
    Function* f = CS.getCalledFunction();
    int callers = 0;
    for (auto User: f->users()) {
      callers++;
    }

    return callers == 1;
  }

  void test(std::set<Instruction*>& set) {
    for (auto I: set) {
      CallSite S = CallSite(I);
      Function* f = S.getCalledFunction();
      int size = 0;
      for (auto User: f->users()) {
        size++;
      }
      _callers[f] = size;
    }

  }

  void doCallReplacement(Module& M) {
    if (Function* F = M.getFunction("malloc")) {
      F->setName("ctx_malloc");
    }

    if (Function* F = M.getFunction("calloc")) {
      F->setName("ctx_calloc");
    }

    if (Function* F = M.getFunction("realloc")) {
      F->setName("ctx_realloc");
    }

    if (Function* F = M.getFunction("free")) {
      F->setName("ctx_free");
    }
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    appendNewGlobal(M);

    std::set<Instruction*> l0_sites;
    get_first_level_callsites(M, l0_sites);
    //test(l0_sites);
    instrumentCallsites(l0_sites, 0);
    std::set<Instruction*> callees = l0_sites;
    int level = 1;
    while (level <= _nlevel) {
      std::set<Instruction*> callers;
      get_upper_level_callsites(callees, callers);
      /* Do instrument */
      instrumentCallsites(callers, level);
      //test(callers);
      callees = callers;
      level++;
    }

    if (_replace_calls) {
      doCallReplacement(M);
    }
    
    printStats();
    //printBBInstrumentInfo();
    return true;
  }
};

char CtxVarPass::ID = 0;

static RegisterPass<CtxVarPass> CtxVarPassInfo("cv", "Context Variable Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif //LLVM_Ben_H
