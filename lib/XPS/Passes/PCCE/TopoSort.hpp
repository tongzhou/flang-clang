//
// Created by tzhou on 2/23/18.
//

#ifndef LLVM_TOPOSORT_H
#define LLVM_TOPOSORT_H


#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>
#include <Utils/CallGraphBase.h>

typedef std::string string;

using namespace llvm;

namespace xps {
class TopoSort {
public:
  bool _use_ics;
  Module *_m;
  std::map<Value*, int> _visited;
  std::set<Instruction*> _back_edges;
  std::vector<Function*> _stack;
public:
  TopoSort() {

  }

  bool isVisited(Value* V) {
    assert(_visited.find(V) != _visited.end());
    return _visited[V] != false;
  }

  void sort(std::vector<Function*>& ret, Module& M) {
    for (auto& F: M) {
      _visited[&F] = 0;
    }

    // for (auto& F: M) {
    //   if (!has_definition(&F)) {
    //     continue;
    //   }

    //   if (!isVisited(&F)) {
    //     sortImpl(&F, M);
    //   }
    // }
    sortImpl(get_main_function(&M), M);

    ret = _stack;
  }

  void sortImpl(Function* F, Module& M) {
    _visited[F] = true;

    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
          if (!has_definition(callee)) {
            continue;
          }

          if (isVisited(callee)) {
            continue;
          }

          sortImpl(callee, M);
        }
      }
    }

    _stack.push_back(F);
  }
};
}

#endif //LLVM_TOPOSORT_H
