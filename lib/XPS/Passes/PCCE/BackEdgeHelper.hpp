//
// Created by tzhou on 2/23/18.
//

#ifndef LLVM_BACKEDGEHELPER_H
#define LLVM_BACKEDGEHELPER_H


#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>

typedef std::string string;

using namespace llvm;

namespace xps {
class BackEdgeHelper {
public:
  Module *_m;
  int _time;
  std::vector<int> _counts;
  std::map<Value*, int> _starts;
  std::map<Value*, int> _finishes;
  std::set<Instruction*> _back_edges;
public:
  BackEdgeHelper() {
    _time = 0;
    _counts.assign(10, 0);
  }

  bool isVisited(Value* V) {
    if (_starts.find(V) == _starts.end()) {
      V->dump();
    }
    assert(_starts.find(V) != _starts.end());
    return _starts[V] != -1;
  }

  bool isFinished(Value* V) {
    assert(_finishes.find(V) != _finishes.end());
    return _finishes[V] != -1;
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }

  void report() {
    outs() << "total edges: " << _counts[0] << "\n"
           << "total functions: " << _counts[1] << "\n"
           << "back edges: " << _back_edges.size()  << "\n"
      ;
  }

  void markBackEdge(std::set<Instruction*>& set, Module& M) {
    for (auto& F: M) {
      _starts[&F] = -1;
      _finishes[&F] = -1;
    }

    auto main = get_main_function(&M);
    assert(main);
    markBackEdgeImpl(main, M);
    set = _back_edges;
  }

  void markBackEdgeImpl(Function* F, Module& M) {
    if (isVisited(F)) {
      return;
    }

    _starts[F] = _time++;
    _counts[1]++;
    if (F->isDeclaration()) {
      goto FINISH;
    }

    if (!has_definition(F)) {
      goto FINISH;
    }


    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
          if (!has_definition(callee)) {
            continue;
          }

          _counts[0]++;

          /* New back edge */
          if (isVisited(callee)) {
            if (!isFinished(callee)) {
              _back_edges.insert(&I);
            }
          }
          else {
            markBackEdgeImpl(callee, M);
          }


        }
      }
    }

    FINISH:
    _finishes[F] = _time++;
  }
};
}

#endif //LLVM_BACKEDGEHELPER_H
