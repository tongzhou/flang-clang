//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_PCCE_H
#define XPS_PCCE_H

#include <cmath>
#include <set>
#include <fstream>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <fcntl.h>
#include "BackEdgeHelper.hpp"
#include "TopoSort.hpp"
#include <Utils/CallGraphBase.h>

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<string> QCaller("caller");
static cl::opt<string> QCallee("callee");
static cl::opt<bool> DoBE("back-edge", cl::init(true));
static cl::opt<bool> Baseline("baseline");
static cl::opt<bool> PrintPaths("print-paths");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> PrintNumCC("print-num-cc");

static cl::opt<bool> CheckMode("check-mode", cl::init(false));
static cl::opt<bool> AcyclicMode("acyclic-mode", cl::init(false));

// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class PCCEPass: public ModulePass, public XPSTypeCache {
public:
  int _time = 0;
  int _max_level = 0;
  int _be_counter = 0;
  Type* _ctx_id_ty;
  bool _verbose = true;
  bool _opt_zero_inc = true;

  // Debug
  int _mycount1 = 0;

  Module* _m = nullptr;
  std::vector<size_t> _ids;
  std::vector<size_t> _counts;
  std::vector<Function*> _topo;
  std::map<Function*, double > _num_cc;
  std::map<Function*, int> _func_levels;
  std::set<Instruction*> _back_edges;
  std::set<Function*> _root_nodes;
  std::map<Function*, int> _callers;
  std::set<string> _query_funcs;
  std::ofstream _log;

public:
  static char ID;

  PCCEPass(): ModulePass(ID) {
    _counts.assign(10, 0);

    if (Baseline) {
      _opt_zero_inc = false;
    }
  }

  ~PCCEPass() {

  }

  void initQueryFunctions() {
    _query_funcs.insert("malloc");
    _query_funcs.insert("calloc");
    _query_funcs.insert("realloc");
    _query_funcs.insert("free");
  }

  bool isQueryFunction(Function* F) {
    return _query_funcs.find(F->getName().str())
        != _query_funcs.end();
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }

  void printCallers(Function* F) {
    for (auto I: get_callers(F)) {
      dumpI(I);
    }
    outs() << "\n";
  }

  void annotateRecursive() {
    int callers = 0;
    auto main = get_main_function(_m);
    assert(main);
    _root_nodes.insert(main);
    for (auto I: _back_edges) {
      _root_nodes.insert(get_callee(CallSite(I)));
    }

    for (auto F: _topo) {
      _num_cc[F] = 0;
    }

    //_num_cc[main] = 0;

    double max_num_cc = 0;
    for (int i = _topo.size()-1; i >= 0; --i) {
      auto f = _topo[i];
      if (PrintNumCC)
        outs() << f->getName() << " ";

      if (AcyclicMode) {
        if (f == main) {
          _num_cc[f]++;
        }
      }
      else {
        if (_root_nodes.find(f) != _root_nodes.end()) {
          _num_cc[f]++;
        }
      }
      
      //outs() << "id: " << i << ", callers: " << get_callers(f).size() << "\n";
      

      for (auto I: get_callers(f)) {
        if (_back_edges.find(I) != _back_edges.end()) {
          continue;
        }
        
        auto p = I->getFunction();
        if (_num_cc.find(p) == _num_cc.end()) {
          continue;
        }
        
        _num_cc[f] += _num_cc[p];
        /* if (i == 9) { */
        /*   dumpI(I); */
        /*   outs() << "_num_cc[p]: " << _num_cc[p] << "\n"; */
        /*   outs() << "_num_cc[f]: " << _num_cc[f] << "\n"; */
        /* } */

      }

      if (_num_cc[f] > max_num_cc) {
        max_num_cc = _num_cc[f];
      }
      
      if (PrintNumCC) {
        outs() << _num_cc[f] << "\n";
      }

      outs() << "id: " << i << ", numcc: " << (size_t)_num_cc[f] << "\n";
    }

    outs() << "max cc num: " << max_num_cc << " " << int(ceil(log2(max_num_cc))) << " numccsize: " << _num_cc.size() << "\ncc bytes:";
    if (max_num_cc < 256) {
      outs() << "(1 byte)\n";
    }
    else if (max_num_cc < 65536) {
      outs() << "(2 bytes)\n";
    }
    else if (max_num_cc < 4294967296) {
      outs() << "(4 bytes)\n";
    }
    else {
      outs() << "(8 bytes)\n";
    }
  }

//  void annotate() {
//    for (auto& F: *_m) {
//      _num_cc[&F] = 0;
//    }
//
//    auto main = get_main_function(_m);
//    assert(main);
//    _num_cc[main] = 1;
//
//    for (int i = _topo.size()-1; i >= 0; --i) {
//      auto f = _topo[i];
//      //outs() << f->getName() << " ";
//      for (auto I: get_callers(f)) {
//        auto p = I->getFunction();
//        _num_cc[f] += _num_cc[p];
//      }
//      //outs() << _num_cc[f] << "\n";
//    }
//  }

  void instrumentCallSites() {
    int count = 0;
    for (auto& F: *_m) {
      if (_num_cc[&F] == 0) {
        continue;
      }

      if (!has_definition(&F)) {
        continue;
      }

      size_t s = 0;
      if (_root_nodes.find(&F) != _root_nodes.end()) {
        s = 1;
      }


      for (auto I: get_callers(&F)) {
        auto p = I->getFunction();
        if (_num_cc[p] == 0) {
          continue;
        }

        count++;
        if (!shouldInstrument(CallSite(I))) {
          continue;
        }

        if (isBackEdge(I)) {
          if (!AcyclicMode) {
            doBackEdge(CallSite(I));
          }
         
        }
        else {
          doNonBackEdge(CallSite(I), &F, s);
          s += _num_cc[p];
        }
      }


    }
    outs() << "site count: " << count << "\n";
  }

  bool shouldInstrument(CallSite CS) {
    return true;
  }

  void doBackEdge(CallSite CS) {
    if (!DoBE) {
      return;
    }

    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    auto callee = get_callee(CS);

    /* if (dyn_cast<InvokeInst>(I)) { */
    /*   _mycount1++; */
    /*   if (_mycount1 > 0) { */
    /*     return; */
    /*   } */
    /* } */

    //dumpI(I);
    
    _be_counter++;
    assert(_be_counter < 65536);


    if (_be_counter == 2548 || _be_counter == 2922
        ) {
    //if (_be_counter == 435 || _be_counter == 522) {
      dumpI(I);
      //return;
    }


    if (get_callee(CS)->getName().equals("Perl_die")) {
      //return;
    }

    std::vector<Value*> args{ ConstantInt::get(Int16Ty, _be_counter) };
    Function* xps_push_ctx = _m->getFunction("pcce_push_ctx");
    CallInst::Create(xps_push_ctx, args, "", I);

    std::vector<Value*> args1{};
    Function* xps_pop_ctx = _m->getFunction("pcce_pop_ctx1");

    for (auto s: get_succ_insts(I)) {
      CallInst::Create(xps_pop_ctx, args, "", s);
    }

    // Stats stuff
    _counts[2]++;

  }

  void doNonBackEdge(CallSite CS, Function* callee, size_t s) {
    if (!has_definition(callee)) {
      return;
    }

    
    
    auto I = CS.getInstruction();
    if (InvokeInst* ii = dyn_cast<InvokeInst>(I)) {
      
      _mycount1++;
      
      //if (_mycount1 > 249 && _mycount1 < 251) {
      //if (_mycount1 > 299) {
        //dumpI(I);
        //return;
      //}
      //dumpI(I);
      //ii->getNormalDest()->dump();
    }

    

    if (CS.isIndirectCall()) {
      outs() << I->getFunction()->getName() << " -> "
             << callee->getName() << "\n";
    }
    assert(!isBackEdge(I));

    if (s == 0 && _opt_zero_inc) {
      return;
    }

    Function* caller = I->getFunction();
    insertIncrement(I, s);

    for (auto SI: get_succ_insts(I)) {
      insertDecrement(SI, s);
    }

    _counts[1]++;
  }

  void insertIncrement(Instruction* I, size_t value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto add = BinaryOperator::Create(Instruction::Add,
                                          load, v, "", I);
    auto store = new StoreInst(add, ctx_id, I);
  }

  void insertDecrement(Instruction* I, size_t value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto sub = BinaryOperator::Create(Instruction::Sub,
                                      load, v, "", I);
    auto store = new StoreInst(sub, ctx_id, I);
  }

  void appendNewGlobal() {
    auto gv = new GlobalVariable(*_m, _ctx_id_ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "pcce_id", nullptr);
    insertPushAndPopDecl();
  }

  void insertPushAndPopDecl() {
    std::vector<Type*> arg_types{ Int16Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "pcce_push_ctx", _m);

    std::vector<Type*> arg_types1{ };
    FunctionType *FT1 = FunctionType::get(VoidTy, arg_types1, false);
    Function::Create(FT1, Function::ExternalLinkage, "pcce_pop_ctx", _m);

    std::vector<Type*> void_arg_type{ };
    std::vector<Type*> int16_arg_type{ Int16Ty };
    std::vector<Type*> int32_arg_type{ Int32Ty };

    FunctionType *FT_void_void = FunctionType::get(VoidTy, void_arg_type, false);
    FunctionType *FT_void_int16 = FunctionType::get(VoidTy, int16_arg_type, false);
    FunctionType *FT_void_int32 = FunctionType::get(VoidTy, int32_arg_type, false);

    Function::Create(FT_void_int16, Function::ExternalLinkage, "pcce_pop_ctx1", _m);
  }

  void printQueryFuncStats() {
    for (auto name: _query_funcs) {
      Function* f = _m->getFunction(name);
      if (!f) {
        continue;
      }
      outs() << "[" << name << "]\n"
             << "  num_cc: " << _num_cc[f] << '\n'
          ;
    }
  }

  void printIDStats() {
    outs() << "[ids]\n";
    for (int i = 0; i < _ids.size(); ++i) {
      outs() << "  " << i << ": " << _ids[i] << '\n';
    }
  }

  void printStats() {
    printIDStats();
    errs() << "max: " << _max_level << "\n";
    errs() << "instrumented non-back edges: " << _counts[1] << "\n";
    errs() << "instrumented back edges: " << _counts[2] << "\n";

    printQueryFuncStats();
    //printStatsToFile();
  }

  void printStatsToFile() {
    FILE* f = fopen("cr-funcs.txt", "w");
    raw_fd_ostream OS(fileno(f), true);
    for (auto it: _func_levels) {
      Function* f = it.first;
      int l = it.second;
      if (l == -1) {
        continue;
      }
      OS << f->getName() << " " << l << "\n";
    }
    OS.close();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;

    _ctx_id_ty = Int64Ty;
    TopoSort ts;
    ts.sort(_topo, M);

    initQueryFunctions();
    BackEdgeHelper beh;
    beh.markBackEdge(_back_edges, M);
    beh.report();
    
    appendNewGlobal();
    annotateRecursive();
    instrumentCallSites();

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char PCCEPass::ID = 0;


static RegisterPass<PCCEPass> PCCEPassInfo("pcce", "PCCE Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif
