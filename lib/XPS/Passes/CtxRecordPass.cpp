//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_CtxRecord_H
#define XPS_CtxRecord_H

#include <cmath>
#include <set>
#include <fstream>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include "BenPass.h"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<int> CtxLevel("ctx-level", cl::desc(""));

static cl::opt<bool> InitCV("init-cv", cl::desc(""));

static cl::opt<string> CVLogFile("cv-log", cl::desc(""));


// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class CtxRecordPass: public ModulePass, public XPSTypeCache {
public:
  Module* _m = nullptr;
  std::vector<size_t> _site_ids = {0, 0, 0, 0};
  std::map<Function*, int> _callers;
  std::map<BasicBlock*, std::set<Instruction*>> _bb;
  std::ofstream _log;
  bool _verbose = false;
  bool _record_cv = false;
  bool _opt_reloads = false;
  bool _opt_singular_calls = false;
  
public:
  static char ID;

  CtxRecordPass(): ModulePass(ID) {
    outs() << "Construct CtxRecordPass...\n";
    if (!CVLogFile.empty()) {
      _record_cv = true;
      _log.open(CVLogFile);
    }
    outs() << "nlevel: " << CtxLevel << "\n";
  }

  ~CtxRecordPass() {
    outs() << "Destruct CtxRecordPass...\n";
    if (_record_cv) {
      _log.close();
    }
  }

  void getTargetFunctions(Module& module, std::set<Function*>& funcs) {
    BenPass* ben = new BenPass();
    for (auto i: ben->_alloc_set) {
      getCallers(module, i->old_name, funcs, CtxLevel);
    }
  }

  void getCallers(Module& module, string& alloc,
                  std::set<Function*>& funcs, int nlevel) {
    Function* F = module.getFunction(alloc);

    if (!F) {
      return;
    }

    std::set<Function*> old_set;
    old_set.insert(F);

    while (nlevel--) {
      std::set<Function*> new_set;
      for (auto f: old_set) {
        for (auto u: f->users()) {
          if (CallInst* ci = dyn_cast<CallInst>(u)) {
            new_set.insert(ci->getFunction());
          }
        }
        funcs.insert(f);
      }

      old_set = new_set;
    }

    for (auto i: old_set) {
      funcs.insert(i);
    }
  }

  void addBBInstrumentInfo(Instruction* I) {
    BasicBlock* BB = I->getParent();
    if (_bb.find(BB) == _bb.end()) {
      _bb[BB] = std::set<Instruction*>();
    }

    _bb[BB].insert(I);
  }

  void printBBInstrumentInfo() {
    for (auto it: _bb) {
      BasicBlock* BB = it.first;
      auto sites = it.second;
      outs() << "\n " << sites.size() << '\n';
    }
  }

  size_t getLevelValue(size_t value, int level) {
    int shift = level * 16;
    return (value << shift);
  }

  void insertStore(Instruction* I, int lv) {
    auto ctx_var = _m->getGlobalVariable("xps_ctx");
    Value* idx[2];
    idx[0] = ConstantInt::get(Int32Ty, 0);
    idx[1] = ConstantInt::get(Int32Ty, lv);
    auto gep = GetElementPtrInst::Create(nullptr, ctx_var, idx, "", I);
    auto c = ConstantInt::get(Int16Ty, _site_ids[lv]);
    auto store = new StoreInst(c, gep, I);
  }

  void instrumentCallsites(std::set<Instruction*>& sites, int level) {
    auto ctx_var = _m->getGlobalVariable("xps_ctx");
    assert(ctx_var);
    for (auto I: sites) {
      CallSite S = CallSite(I);
      assert(S && "I should be a callsite");
      if (_opt_singular_calls) {
        if (isSingularCall(S)) {
          continue;
        }
      }

      insertStore(I, level);
      
      /* Do some bookkeepping */
      if (_record_cv) {
        _log << level
             << " " << "value"
             << " " << I->getFunction()->getName().str()
             << "\n";
      }

      _site_ids[level]++;
    }
  }

  void appendNewGlobal(Module& M) {
    ArrayType *ty = ArrayType::get(Int16Ty, 4);
    auto gv = new GlobalVariable(M, ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "xps_ctx", nullptr,
                                 GlobalVariable::GeneralDynamicTLSModel);
  }

  bool isSingularCall(CallSite CS) {
    Function* f = CS.getCalledFunction();
    int callers = 0;
    for (auto User: f->users()) {
      callers++;
    }

    return callers == 1;
  }

  void test(std::set<Instruction*>& set) {
    for (auto I: set) {
      CallSite S = CallSite(I);
      Function* f = S.getCalledFunction();
      int size = 0;
      for (auto User: f->users()) {
        size++;
      }
      _callers[f] = size;
    }

  }

  void doCallReplacement(Module& M) {
    if (Function* F = M.getFunction("malloc")) {
      F->setName("ctx_malloc");
    }

    if (Function* F = M.getFunction("calloc")) {
      F->setName("ctx_calloc");
    }

    if (Function* F = M.getFunction("realloc")) {
      F->setName("ctx_realloc");
    }

    if (Function* F = M.getFunction("free")) {
      F->setName("ctx_free");
    }
  }
  
  void printStats() {
    int counter = 0;
    for (auto it: _callers) {
      outs() << it.first->getName()
             << " " << it.second << '\n';
      if (it.second == 1) {
        counter++;
      }
    }

    outs() << _callers.size()
           << " " << counter << '\n';
    for (int i = 0; i < 4; ++i) {
      outs() << "_site_ids[" << i << "]: "
             << _site_ids[i] << "\n";
    }
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    appendNewGlobal(M);

    std::set<Instruction*> l0_sites;
    get_first_level_callsites(M, l0_sites);

    instrumentCallsites(l0_sites, 0);
    std::set<Instruction*> callees = l0_sites;
    int level = 1;
    while (level <= CtxLevel) {
      std::set<Instruction*> callers;
      get_upper_level_callsites(callees, callers);
      /* Do instrument */
      instrumentCallsites(callers, level);

      callees = callers;
      level++;
    }

    printStats();
    //printBBInstrumentInfo();
    return true;
  }
};

char CtxRecordPass::ID = 0;


static RegisterPass<CtxRecordPass> CtxRecordPassInfo("cr", "Context Record Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif

