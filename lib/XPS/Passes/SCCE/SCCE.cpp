//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_SCCE_HPP
#define XPS_SCCE_HPP

#include <cmath>
#include <set>
#include <fstream>
#include <bitset>
#include <fcntl.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/IR/LegacyPassManager.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Passes/PCCE/BackEdgeHelper.hpp>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "Utils/SCC.hpp"


using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<bool> PrintLevels("print-levels");
static cl::opt<bool> PrintStats("print-stats");
  
class SCCEPass: public ModulePass, public XPSTypeCache {
public:
  Module* _m = nullptr;
  SCCGraph* _scc = nullptr;
  std::map<int, int> _scc_levels;
  std::map<int, int> _level_bits;
  std::map<Instruction*, string> _level_codes;
  std::map<Instruction*, string> _edges_codes;

  // PCCE stuff
  std::set<int> _root_nodes;
  std::map<int, double> _num_cc;

  int _time = 0;
  int _max_level = 0;
  size_t _counts[10];

  bool _opt_zero_inc = 1;
public:
  static char ID;

  SCCEPass(): ModulePass(ID) {
  }

  ~SCCEPass() {

  }

  void setEdgeCode(Instruction* I, string code) {
    _edges_codes[I] = code;
  }

  string getEdgeCode(Instruction* I) {
    assert(_edges_codes.find(I) != _edges_codes.end());
    return _edges_codes[I];
  }

  void setLevelCode(Instruction* I, string code) {
    
    _level_codes[I] = code;
  }

  string getLevelCode(Instruction* I) {
    assert(_level_codes.find(I) != _level_codes.end());
    return _level_codes[I];
  }

  void calcSCCLevels() {
    int callers = 0;
    double max_num_cc = 0;
    outs() << "sccs: " << _scc->_scc_id << "\n";
    auto main = get_main_function(_m);
    assert(main);
    _root_nodes.insert(_scc->getSCCID(main));

    for (int id = _scc->_scc_id-1; id >= 0; --id) {
      std::vector<Instruction*> edges;
      _scc->getPredEdges(id, edges);
      // The sketch of this calculation
      // Associate each SCC group with a context level
      // Associate each edge with an edge code and a level code
      
      
      // Construct caller_level_counts
      std::map<size_t, std::vector<Instruction*> > level_groups;
      for (auto I: edges) {
        Function* caller = I->getFunction();
        int caller_scc  = _scc->getSCCID(caller);
        int caller_level = _scc_levels[caller_scc];
        if (level_groups.find(caller_level) == level_groups.end()) {
          level_groups[caller_level] = std::vector<Instruction*>();
        }
        level_groups[caller_level].push_back(I);
      }

      int level_bits = ceil(log2(level_groups.size()));
      int max_total_bits = 0;
      int level_counts = 0;
      for (auto it: level_groups) {
        int caller_level = it.first;
        std::vector<Instruction*> edges = it.second;
        for (int k = 0; k < edges.size(); ++k) {
          Instruction* I = edges[k];
          string level_code = getBitsForInt(level_counts, level_bits);
          int edge_bits = ceil(log2(edges.size()));
          string edge_code = getBitsForInt(k, edge_bits);
          setLevelCode(I, level_code);
          setEdgeCode(I, edge_code);
          int total_bits = caller_level + edge_bits + level_bits;
          if (total_bits > max_total_bits) {
            max_total_bits = total_bits;
          }
          
        }
        level_counts++;
      }
      

      // Iterate all callers and find out the highest 
      // context level among them
      // int longest = 0;
      // for (auto i: caller_level_counts) {
      //   auto level = i.first;
      //   auto edge_num = i.second;
      //   outs() << level << " " << edge_num << "\n";
      //   int path_len = level+ceil(log2(edge_num));

      //   if (path_len > longest) {
      //     longest = path_len;
      //   }
      // }
      
      //outs() << "long: " << max_total_bits << "\n";

      //_scc_levels[id] = highest + ceil(log2(edges.size()));
      _scc_levels[id] = max_total_bits;      

      if (_scc_levels[id] > _max_level) {
        _max_level = _scc_levels[id];
      }


      //========== Calculate PCCE numCC as well ==============
      // This part is mainly for debugging      
      // if (_root_nodes.find(id) != _root_nodes.end()) {
      //   _num_cc[id]++;
      // }
      // outs() << id << " " << _num_cc[id] << "\n";

      // for (auto I: edges) {
      //   _num_cc[id] += _num_cc[caller_scc];
      // }

      // if (_num_cc[id] > max_num_cc) {
      //   max_num_cc = _num_cc[id];
      // }
      //======================================================

      
      // outs() << "id: " << id << " "
      //        << "numCC: " << _num_cc[id] << " "
      //        << "CL: " << _scc_levels[id] << " "
      //        << "\n";
    }

    //if (PrintLevels) {
      // for (int id = _scc->_scc_id-1; id >= 0; --id) {
      //   //_scc->printSCCGroup(id);
      //   outs() << "  level: " << _scc_levels[id]
      //          << "  bits: " << _level_bits[_scc_levels[id]]
      //          << "\n";
      // }


    outs() << "total bits: " << _max_level << "\n";
    outs() << "max cc num: " << max_num_cc << " " << int(ceil(log2(max_num_cc))) << " numccsize: " << _num_cc.size() << "\ncc bytes:";
  }

  int getFunctionLevel(Function* F) {
    return _scc_levels[_scc->getSCCID(F)];
  }

  string getBitsForInt(int v, int len) {
    assert(len <= 32);
    string bits = std::bitset<32>(v).to_string();
    string subbits = bits.substr(32-len);
    

    // outs() << "len: " << len << "\n";
    // outs() << bits << " " << subbits << "\n";
    // outs() << "bitslen: " << subbits.size() << "\n";
    return subbits;
  }

  void instrumentCallSites() {
    int iedge_count = 0;
    int iedge_id = 1;
    
    for (int id = _scc->_scc_id-1; id >= 0; --id) {
      std::vector<Instruction *> edges;
      _scc->getPredEdges(id, edges);
      int caller_id = 0;

      for (auto I: edges) {
        //doExternalCallSite_64bits(CallSite(I));
        doExternalCallSite_192bits(CallSite(I));
      }
      
    }
  }

  void doExternalCallSite_192bits(CallSite CS) {
    auto callee = get_callee(CS);
    if (!has_definition(callee)) {
      return;
    }

    auto I = CS.getInstruction();

    Function* caller = I->getFunction();
    int caller_level = getFunctionLevel(caller);
    int callee_level = getFunctionLevel(callee);

    checkLevels(I, caller_level, callee_level);
    
    if (caller_level == callee_level) {
      return;
    }

    string level_code = getLevelCode(I);
    string edge_code = getEdgeCode(I);
    //outs() << level_code << " " << edge_code << "\n";

    size_t levelv = 0;
    size_t edgev = 0;
    int level_value_pos = (callee_level - level_code.size());
    int edge_value_pos = caller_level;

    setIds(CS, level_code, level_value_pos);
    setIds(CS, edge_code, edge_value_pos);
  }

  size_t binaryStrToLL(string code) {
    if (code.empty()) {
      return 0;
    }
    else {
      return std::stoll(code, nullptr, 2);
    }
  }

  void setIds(CallSite CS, string code, int shift) {
    if (code.empty()) {
      return;
    }
    int start = 0 + shift;
    int end = code.size() + shift;
    
    assert(end < 192 && "We only 3 64-bit ids suffice for all benchmarks");
    if (start >= 128) {
      int in_id_shift = start - 128;
      doNonBackEdge(CS, binaryStrToLL(code) << in_id_shift,
                    _m->getGlobalVariable("scce_ctx2"));
    }
    else if (start < 128 && end >= 128) {
      string high_part = code.substr(0, end - 128);
      string low_part = code.substr(end - 128);
      int high_shift = 0;
      int low_shift = start;
      while (low_shift >= 64) {
        low_shift -= 64;
      }


      doNonBackEdge(CS, binaryStrToLL(high_part) << high_shift,
                    _m->getGlobalVariable("scce_ctx2"));
      
      doNonBackEdge(CS, binaryStrToLL(low_part) << low_shift,
                    _m->getGlobalVariable("scce_ctx1"));
    }
    else if (end < 128 && start >= 64) {
      int in_id_shift = start - 64;
      doNonBackEdge(CS, binaryStrToLL(code) << in_id_shift,
                    _m->getGlobalVariable("scce_ctx1"));
    }
    else if (start < 64 && end >= 64) {
      string high_part = code.substr(0, end - 64);
      string low_part = code.substr(end - 64);
      int high_shift = 0;
      int low_shift = start;

      doNonBackEdge(CS, binaryStrToLL(high_part) << high_shift,
                    _m->getGlobalVariable("scce_ctx1"));

      doNonBackEdge(CS, binaryStrToLL(low_part) << low_shift,
                    _m->getGlobalVariable("scce_ctx0"));
    }
    else if (end < 64) {
      doNonBackEdge(CS, binaryStrToLL(code) << start,
                    _m->getGlobalVariable("scce_ctx0"));
    }
    else {
      assert(0);
    }
  }
  
  void doExternalCallSite_64bits(CallSite CS) {
    auto callee = get_callee(CS);
    if (!has_definition(callee)) {
      return;
    }

    auto I = CS.getInstruction();

    Function* caller = I->getFunction();
    int caller_level = getFunctionLevel(caller);
    int callee_level = getFunctionLevel(callee);

    checkLevels(I, caller_level, callee_level);
    
    if (caller_level == callee_level) {
      return;
    }

    string level_code = getLevelCode(I);
    string edge_code = getEdgeCode(I);
    //outs() << level_code << " " << edge_code << "\n";

    size_t levelv = 0;
    size_t edgev = 0;
    int level_value_pos = (callee_level - level_code.size());
    int edge_value_pos = caller_level;
    if (!level_code.empty()) {
      levelv = binaryStrToLL(level_code);
      levelv <<= level_value_pos;
    }

    if (!edge_code.empty()) {  
      edgev = binaryStrToLL(edge_code);
      edgev <<= edge_value_pos;
    }
    
    if (!level_code.empty()) {
      // outs() << level_value_pos << " " << edge_value_pos << "\n";
      // outs() << (void*)levelv << " " << (void*)edgev << "\n";
    }

    
    size_t append_value = levelv | edgev;
    if (append_value != levelv + edgev) {
      outs() << level_code << " " << edge_code << "\n";
      outs() << level_value_pos << " " << edge_value_pos << "\n";
    }
    assert(append_value == levelv + edgev);
    doNonBackEdge(CS, append_value, _m->getGlobalVariable("scce_ctx0"));
    
  }

  void doNonBackEdge(CallSite CS, size_t s, GlobalVariable* ctx_id) {
    auto I = CS.getInstruction();

    if (s == 0 && _opt_zero_inc) {
      return;
    }

    Function* caller = I->getFunction();
    insertIncrement(I, s, ctx_id);

    for (auto SI: get_succ_insts(I)) {
      insertDecrement(SI, s, ctx_id);
    }

    _counts[1]++;
  }

  void insertIncrement(Instruction* I, size_t value, GlobalVariable* ctx_id) {
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(Int64Ty, value);
    auto add = BinaryOperator::Create(Instruction::Add,
                                          load, v, "", I);
    auto store = new StoreInst(add, ctx_id, I);
  }

  void insertDecrement(Instruction* I, size_t value, GlobalVariable* ctx_id) {
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(Int64Ty, value);
    auto sub = BinaryOperator::Create(Instruction::Sub,
                                      load, v, "", I);
    auto store = new StoreInst(sub, ctx_id, I);
  }


  void checkLevels(Instruction* I, int caller_level, int callee_level) {
    // if (_instr_verbose) {
    //   outs() << "caller lv: " << caller_level << " "
    //          << "callee lv: " << callee_level << "\n"
    //     ;
    // }

    if (caller_level > callee_level) {
      errs() << "bad scc found:\n "
             << "caller lv: " << caller_level << " "
             << "callee lv: " << callee_level << "\n"
        ;
      dumpI(I);
    }
    assert(caller_level <= callee_level);
  }

  void appendNewGlobals() {
//     ArrayType *array_ty = ArrayType::get(_ctx_ty, CtxSize);
//     ArrayType *i64_arr_ty = ArrayType::get(Int64Ty, CtxSize);

//     // Add one more param GlobalVariable::GeneralDynamicTLSModel
//     // to make it thread-local
//     new GlobalVariable(*_m, array_ty, false,
//                                  GlobalValue::ExternalLinkage,
//                                  nullptr, "scce_ctx", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_ctx0", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_ctx1", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_ctx2", nullptr);
    new GlobalVariable(*_m, Int64Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "scce_ctx3", nullptr);
    insertFuncDecl();
  }

  void insertFuncDecl() {
  }

  void printStats() {
    outs() << "[stats]\n";
    outs() << "  max: " << _max_level << "\n";
    // outs() << "  size: " << CtxSize << "\n";
    // outs() << "  sites: " << _counts[0] << "\n";
    // outs() << "  tree edge stores: " << _counts[1] << "\n";
    // outs() << "  cycle edge stores: " << _counts[3] << "\n";
    // outs() << "  simple sccs: " << _counts[2] << "\n";


    //printBackEdgeStats();
    //printStatsToFile();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    _scc = new SCCGraph(_m);
    _scc->markSCC();
    //_scc->markSimpleSCCs();
    //_scc->printSCCID();
    _scc->printStats();
    
    calcSCCLevels();
    // printPOILevel();
    appendNewGlobals();
    instrumentCallSites();

    return true;
  }
};

char SCCEPass::ID = 0;


static RegisterPass<SCCEPass> SCCEPassInfo("scce", "Slot-based Calling Context Encoding Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif
