//
// Created by tzhou on 1/21/18.
//

#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"
#include "Utils/XPSTypeCache.h"

using namespace llvm;

namespace xps {

typedef std::string string;

class ProfileCallSitePass : public ModulePass {
  int _id;
  Module* _m;
  XPSTypeCache _tc;
  Function* _extern_func;
  std::ofstream _ofs;
public:
  static char ID;
public:
  ProfileCallSitePass(): ModulePass(ID) {
    _id = 0;
    _ofs.open("site-ids.txt");
  }

  ~ProfileCallSitePass() {
    _ofs.close();
  }

  Type* getInt8PtrTy(LLVMContext& ctx) {
    Type* int8_ty = IntegerType::get(ctx, 8);
    Type* int8_ptr_ty = int8_ty->getPointerTo(0);
    return int8_ptr_ty;
  }

  void insertFunctionDecl() {
    std::vector<Type*> arg_types{ _tc.Int64Ty };
    FunctionType *FT = FunctionType::get(_tc.VoidTy,
                                         arg_types, false);
    _extern_func = Function::Create(FT, Function::ExternalLinkage,
                                    "xps_log_call", _m);
  }

  void doCallSite(CallSite CS) {
    auto I = CS.getInstruction();
    auto site_id = ConstantInt::get(_tc.Int64Ty, _id);
    std::vector<Value*> args;
    args.push_back(site_id);
    CallInst::Create(_extern_func, args, "", I);
    _ofs << _id << " " << I->getFunction()->getName().str() << '\n';
    _id++;
  }

  void doFunction(Function& F) {
    if (F.isDeclaration() || F.isIntrinsic()) {
      return;
    }

    for (auto& B: F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          doCallSite(CS);
        }
      }
    }
  }

  bool runOnModule(Module& M) override {
    _m = &M;
    _tc.initialize(M.getContext());

    insertFunctionDecl();
    for (auto& F: M) {
      doFunction(F);
    }
    return true;
  }
};

char ProfileCallSitePass::ID = 0;

static RegisterPass<ProfileCallSitePass>
    ProfileCallSitePassInfo("profile-cs", "");

}
