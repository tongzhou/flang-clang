//
// Created by tzhou on 12/23/17.
//

#ifndef LLVM_Ben_H
#define LLVM_Ben_H

#include <iostream>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/Constants.h>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "../Utils/XPSTypeCache.h"
#include "../Utils/PassCommons.h"

using namespace llvm;

namespace xps {

typedef std::string string;

struct MFunc {
  string old_name;
  string new_name;
  bool add_id;

  MFunc(string oldname, string newname, bool addid):
    old_name(oldname), new_name(newname), add_id(addid) {}
};

static cl::opt<string> Prefix("prefix");

class AllocPrefixPass: public ModulePass, public XPSTypeCache {
public:
  string _lang;
  std::vector<MFunc*> _alloc_set;
  std::vector<MFunc*> _free_set;
public:
  static char ID;

public:
 AllocPrefixPass(): ModulePass(ID) {
    _lang = "all";
    initLang();
  }

  void initLang() {
    if (_lang == "c" || _lang == "cpp" || _lang == "all") {
      _alloc_set.push_back(new MFunc("malloc", Prefix+"malloc", true));
      _alloc_set.push_back(new MFunc("calloc", Prefix+"calloc", true));
      _alloc_set.push_back(new MFunc("realloc", Prefix+"realloc", true));
      _free_set.push_back(new MFunc("free", Prefix+"free", false));
    }

    if (_lang == "cpp" || _lang == "all") {
      _alloc_set.push_back(new MFunc("_Znam", Prefix+"_Znam", true));
      _alloc_set.push_back(new MFunc("_Znwm", Prefix+"_Znwm", true));
      _free_set.push_back(new MFunc("_ZdaPv", Prefix+"_ZdaPv", false));
      _free_set.push_back(new MFunc("_ZdlPv", Prefix+"_ZdlPv", false));
    }
//
//    if (_lang == "flang" || _lang == "all") {
//      _alloc_set.push_back(new MFunc("f90_alloc", "f90_ben_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_alloc03", "f90_ben_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_alloc03_chk", "f90_ben_alloc03_chk", true));
//      _alloc_set.push_back(new MFunc("f90_alloc04", "f90_ben_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_alloc04_chk", "f90_ben_alloc04_chk", true));
//
//      _alloc_set.push_back(new MFunc("f90_kalloc", "f90_ben_kalloc", true));
//      _alloc_set.push_back(new MFunc("f90_calloc", "f90_ben_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_calloc03", "f90_ben_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_calloc04", "f90_ben_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_kcalloc", "f90_ben_kcalloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc", "f90_ben_ptr_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc03", "f90_ben_ptr_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc04", "f90_ben_ptr_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_alloc03", "f90_ben_ptr_src_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_alloc04", "f90_ben_ptr_src_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_calloc03", "f90_ben_ptr_src_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_calloc04", "f90_ben_ptr_src_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_kalloc", "f90_ben_ptr_kalloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc", "f90_ben_ptr_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc03", "f90_ben_ptr_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc04", "f90_ben_ptr_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_kcalloc", "f90_ben_ptr_kcalloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_allocv", "f90_ben_auto_allocv", true));
//      _alloc_set.push_back(new MFunc("f90_auto_alloc", "f90_ben_auto_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_alloc04", "f90_ben_auto_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_auto_calloc", "f90_ben_auto_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_calloc04", "f90_ben_auto_calloc04", true));
//
//      _free_set.push_back(new MFunc("f90_dealloc", "f90_ben_dealloc", false));
//      _free_set.push_back(new MFunc("f90_dealloc03", "f90_ben_dealloc03", false));
//      _free_set.push_back(new MFunc("f90_dealloc_mbr", "f90_ben_dealloc_mbr", false));
//      _free_set.push_back(new MFunc("f90_dealloc_mbr03", "f90_ben_dealloc_mbr03", false));
//      _free_set.push_back(new MFunc("f90_deallocx", "f90_ben_deallocx", false));
//      _free_set.push_back(new MFunc("f90_auto_dealloc", "f90_ben_auto_dealloc", false));
//      // todo
//    }
  }

  void insert_ben_declarations(Module* module) {
    insert_ben_malloc(module);
    insert_ben_calloc(module);
    insert_ben_realloc(module);
    //insert_ben_free(module);  // do_free will automatically create ben_free declaration
  }

  void insert_ben_malloc(Module* module) {
    std::vector<Type*> arg_types{ Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_malloc", module);
    F->addAttribute(llvm::AttributeSet::ReturnIndex, llvm::Attribute::NoAlias);
  }

  void insert_ben_calloc(Module* module) {
    std::vector<Type*> arg_types{ Int64Ty, Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_calloc", module);
    F->addAttribute(llvm::AttributeSet::ReturnIndex, llvm::Attribute::NoAlias);
  }

  void insert_ben_realloc(Module* module) {
    llvm::LLVMContext& ctx = module->getContext();
    std::vector<Type*> arg_types{ Int8PtrTy, Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_realloc", module);
  }

  void insert_ben_free(Module* module) {
    llvm::LLVMContext& ctx = module->getContext();
    std::vector<Type*> arg_types{ Int8PtrTy };

    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_free", module);
  }

  void doFrees(Module* module) {
    for (auto tf: _free_set) {
      if (llvm::Function* f = module->getFunction(tf->old_name)) {
        f->setName(tf->new_name);
        std::cout << "new name: " << f->getName().str() << '\n';
      }
    }
  }
  
  void doAllocs(Module* module, int id=0) {
    int site_id = 0;
    for (auto mf: _alloc_set) {
      if (auto F = module->getFunction(mf->old_name)) {
        for (auto I: get_callers(F)) {
          CallSite CS = CallSite(I);
            //ci->setCalledFunction(module->getFunction("malloc1"));  // this hangs forever

          if (id == 0) {
            site_id++;
          }
          else {
            site_id = id;
          }
          std::vector<Value*> args_vec {ConstantInt::get(Int32Ty, site_id)};
          for (int h = 0; h < CS.getNumArgOperands(); ++h) {
            args_vec.push_back(CS.getArgOperand(h));
          }

          Function* callee = module->getFunction(mf->new_name);
          CallInst *new_inst = CallInst::Create(callee, args_vec, "");
          new_inst->setAttributes(CS.getAttributes());
          new_inst->copyMetadata(*I);
          if (I->getDebugLoc()) {
            new_inst->setDebugLoc(I->getDebugLoc());
          }

          BasicBlock::iterator ii(I);
          ReplaceInstWithInst(I->getParent()->getInstList(), ii, new_inst);
          break;
        }
      }
    }
  }

  void runOnModule(Module * module) {
    XPSTypeCache::initialize(module->getContext());
    //insert_ben_declarations(module);

    doAllocs(module);
    doFrees(module);
  }
};

char AllocPrefixPass::ID = 0;

static RegisterPass<AllocPrefixPass>
    AllocPrefixPassInfo("alloc-prefix", "");

 
}

#endif //LLVM_Ben_H
